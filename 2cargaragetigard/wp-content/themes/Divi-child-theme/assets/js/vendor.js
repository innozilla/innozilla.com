jQuery(document).ready(function($) {
 
	if ($('.ty-compact-list').length > 1) {
	  $('.ty-compact-list:gt(0)').hide();
	  $('.show-more').show();
	}

	$('.show-more').on('click', function(event) {
        event.preventDefault();
	  //toggle elements with class .ty-compact-list that their index is bigger than 2
	  $('.ty-compact-list:gt(0)').toggle();
	  //change text of show more element just for demonstration purposes to this demo
	  $(this).text() === 'Read More' ? $(this).text('Read Less') : $(this).text('Read More');
    });
    
    //

    if ($('.ty-compact-list0').length > 1) {
        $('.ty-compact-list0:gt(0)').hide();
        $('.show-more0').show();
      }
  
      $('.show-more0').on('click', function(event) {
        event.preventDefault();
        //toggle elements with class .ty-compact-list that their index is bigger than 2
        $('.ty-compact-list0:gt(0)').toggle();
        //change text of show more element just for demonstration purposes to this demo
        $(this).text() === 'Read More' ? $(this).text('Read Less') : $(this).text('Read More');
      });

      //

      if ($('.ty-compact-list1').length > 1) {
        $('.ty-compact-list1:gt(0)').hide();
        $('.show-more1').show();
      }
  
      $('.show-more1').on('click', function(event) {
        event.preventDefault();
        //toggle elements with class .ty-compact-list that their index is bigger than 2
        $('.ty-compact-list1:gt(0)').toggle();
        //change text of show more element just for demonstration purposes to this demo
        $(this).text() === 'Read More' ? $(this).text('Read Less') : $(this).text('Read More');
      });

      //

      if ($('.ty-compact-list2').length > 1) {
        $('.ty-compact-list2:gt(0)').hide();
        $('.show-more2').show();
      }
  
      $('.show-more2').on('click', function(event) {
        event.preventDefault();
        //toggle elements with class .ty-compact-list that their index is bigger than 2
        $('.ty-compact-list2:gt(0)').toggle();
        //change text of show more element just for demonstration purposes to this demo
        $(this).text() === 'Read More' ? $(this).text('Read Less') : $(this).text('Read More');
      });

      //

      if ($('.ty-compact-list3').length > 1) {
        $('.ty-compact-list3:gt(0)').hide();
        $('.show-more3').show();
      }
  
      $('.show-more3').on('click', function(event) {
        event.preventDefault();
        //toggle elements with class .ty-compact-list that their index is bigger than 2
        $('.ty-compact-list3:gt(0)').toggle();
        //change text of show more element just for demonstration purposes to this demo
        $(this).text() === 'Read More' ? $(this).text('Read Less') : $(this).text('Read More');
      });
 
});