<?php



// Exit if accessed directly

if ( !defined('ABSPATH')) exit;



/**

 * Header Template

 *

 *

 * @file           header.php

 * @package        Responsive 

 * @author         Emil Uzelac 

 * @copyright      2003 - 2013 ThemeID

 * @license        license.txt

 * @version        Release: 1.3

 * @filesource     wp-content/themes/responsive/header.php

 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29

 * @since          available since Release 1.0

 */

?>

<!doctype html>
<!-- Just a test -->
<!--[if !IE]>      <html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 9 ]>    <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->

<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>



<meta charset="<?php bloginfo('charset'); ?>" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">



<title><?php wp_title('&#124;', true, 'right'); ?></title>

<link href="<?php bloginfo('template_directory'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<link rel="profile" href="http://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />



<?php wp_head(); ?>

</head>

<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

document,'script','https://connect.facebook.net/en_US/fbevents.js');

 

fbq('init', '1112347235494200');

fbq('track', "PageView");</script>

<noscript><img height="1" width="1" style="display:none"

src="https://www.facebook.com/tr?id=1112347235494200&ev=PageView&noscript=1"

/></noscript>

<!-- End Facebook Pixel Code -->



<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NBSHW4"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-NBSHW4');</script>

<!-- End Google Tag Manager -->





<body <?php body_class(); ?>>





         

    <?php responsive_header(); // before header hook ?>

    <div id="header">



    	<div class="mainhead">

        <div class="topheadbg">

    	<?php if ( get_header_image() != '' ) : ?>

               

        <div id="logo">

            <a href="<?php echo home_url('/'); ?>"><img src="<?php header_image(); ?>" width="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> width;} else { echo HEADER_IMAGE_WIDTH;} ?>" height="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> height;} else { echo HEADER_IMAGE_HEIGHT;} ?>" alt="<?php bloginfo('name'); ?>" /></a>

        </div><!-- end of #logo -->

        

    <?php endif; // header image was removed ?>



    <?php if ( !get_header_image() ) : ?>

                

        <div id="logo">

            <span class="site-name"><a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></span>

            <span class="site-description"><?php bloginfo('description'); ?></span>

        </div><!-- end of #logo -->  



    <?php endif; // header image was removed (again) ?>

<div class="headermenu">



		<?php responsive_header_top(); // before header content hook ?>

    

				<?php wp_nav_menu(array(

				    'container'       => 'div',

						'container_class'	=> 'main-nav',

						'fallback_cb'	  =>  'responsive_fallback_menu',

						'theme_location'  => 'header-menu')

					); 

				?>

                

            <?php if (has_nav_menu('sub-header-menu', 'responsive')) { ?>

	            <?php wp_nav_menu(array(

				    'container'       => '',

					'menu_class'      => 'sub-header-menu',

					'theme_location'  => 'sub-header-menu')

					); 

				?>

            <?php } ?>

        

    <?php responsive_in_header(); // header hook ?>

    

    </div>



<div class="clr"></div>

</div>

			<?php responsive_header_bottom(); // after header content hook ?>

            <!--<div class="header-banner"><img src="<?php //bloginfo('template_directory'); ?>/core/images/header-banner.png" alt=""></div>-->

            <div class="left_form">

                <div class="left_formin">

               <?php if (!dynamic_sidebar('headercontactform')) : endif;?>

                </div>

           		 <div class="clr"></div>

          </div>

          

          

 		</div>

        <div class="header-banner">

        	<div style="margin:0px auto; max-width:1065px;">

            	<?php echo do_shortcode( '[responsive_slider]' ); ?>

                </div>

        </div>

        <!--<div class="sliderbg">

          <div style="margin:0px auto; width:956px; background:#FF0;">asdas</div>

          </div>-->

    </div><!-- end of #header -->

    <div class="headerbottombg"></div>

    <?php responsive_header_end(); // after header container hook ?>

                 

<?php responsive_container(); // before container hook ?>

<div id="container" class="hfeed">



    

    

	<?php responsive_wrapper(); // before wrapper container hook ?>

    <div id="wrapper" class="clearfix">

		<?php responsive_wrapper_top(); // before wrapper content hook ?>

		<?php responsive_in_wrapper(); // wrapper hook ?>