<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;
/**
 Template Name: Q & A Page
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
//get_header(); ?>
<?php include("header-home.php");	 //get_sidebar(); ?>


<style type="text/css">
.question p{ color:#000; font-size:16px;}
.blink_me {
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 1s;
    -webkit-animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;
    
    -moz-animation-name: blinker;
    -moz-animation-duration: 1s;
    -moz-animation-timing-function: linear;
    -moz-animation-iteration-count: infinite;
    
    animation-name: blinker;
    animation-duration: 1s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
}

@-moz-keyframes blinker {  
    0% { opacity: 1.0; }
    50% { opacity: 0.0; }
    100% { opacity: 1.0; }
}

@-webkit-keyframes blinker {  
    0% { opacity: 1.0; }
    50% { opacity: 0.0; }
    100% { opacity: 1.0; }
}

@keyframes blinker {  
    0% { opacity: 1.0; }
    50% { opacity: 0.0; }
    100% { opacity: 1.0; }
}
</style>
<!---------Guage Edit-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/justgage/js/raphael.2.1.0.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/justgage/js/justgage.1.0.1.min.js"></script>
<div id="qanda">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
        <?php //get_template_part( 'loop-header' ); ?>
			<?php responsive_entry_before(); ?>
<div id="widgets" class="grid col-330-1 fit scrolld">
	&nbsp;
	<div id="g1" style="width:500px; height:300px; color:#000;"></div>
</div>
<div id="content" class="col-620">
&nbsp;
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>       
				<?php responsive_entry_top(); ?>
				<?php if(get_post_meta($post->ID, "removetitle", true)=="yes"){?>
                 <h1 class="post-title">&nbsp;</h1>
                 <?php } else {?>                 
                 <h1 class="post-title"><?php the_title(); ?></h1>
                 <?php }?>		
                <div class="post-entry">
					<?php the_content(__('Read more &#8250;', 'responsive')); ?>
					<div class="c_form col-full">

						<?php echo do_shortcode('[contact-form-7 id="507" title="Refund contact form"]'); ?>

					</div>
                    <?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
                </div><!-- end of .post-entry -->
				<?php responsive_entry_bottom(); ?>      
			</div><!-- end of #post-<?php the_ID(); ?> -->       
</div>
			<?php responsive_entry_after(); ?>
<?php $h_titlte = get_post_meta($post->ID, 'Hebriew_title', true);
		$h_marks = get_post_meta($post->ID, 'Hebrriew_marks', true);
		$tax_refund = get_post_meta($post->ID, 'tax_refound', true);
 ?>
        <?php 
		endwhile; 
		get_template_part( 'loop-nav' ); 
	else : 
		get_template_part( 'loop-no-posts' ); 
	endif; 
	?>  
</div><!-- end of #content -->
<script>
      var g1;
	  
	function calcscore(){
    	var score = 0;
		//jQuery(".options li").removeClass("bg_green");
    	jQuery("input[type=radio]:checked").each(function(){
		
		//jQuery(this).parent().addClass("bg_green");
		if(jQuery(this).val() == 999){ //alert(jQuery(this).val());
		score= 0;
		}else{
        score+=parseInt(jQuery(this).val(),10); 
		}
    });
    return score;
	}
	  
	  jQuery("input[type=radio]").change(function(){ 
	  	  var x = calcscore(); //alert(x);
		  calcload(x);
          
      });
	  function calcload(val){
	  	
			//alert(val);
		  if(val >= 50){
		  jQuery("#g1").html("");
			 var g1 = new JustGage({
             id: "g1", 
             value: val,
          min: 0,
          max: 100,
          title: '<?php echo $tax_refund; ?>',
          label: '<?php echo $h_marks; ?>',
		  titleFontColor: "green",
		  });
		  //jQuery("#g1 tspan").fadeOut('fast');
		  //jQuery("#g1 tspan").slideUp( 300 ).delay(300).slideDown( 400 );
		  //delayFunction();
		  startInterval = setInterval(delayFunction, 100);
		  }else{
		  
		  jQuery("#g1").html("");
			 var g1 = new JustGage({
             id: "g1", 
             value: val,
          min: 0,
          max: 100,
          title: '<?php echo $h_titlte; ?>',
          label: '<?php echo $h_marks; ?>'
		  });
		  
		  clearInterval(startInterval);
		  
		  }
		
		}
      
	  function delayFunction() {
		jQuery("#g1 tspan").first().slideUp( 300 ).delay(300).slideDown( 400 );
		}
	  window.onload = function(){ 
        var g1 = new JustGage({
          id: "g1", 
          value: 0,
          min: 0,
          max: 100,
           title: '<?php echo $h_titlte; ?>',
          label: '<?php echo $h_marks; ?>'
        });
      };
	 jQuery(document).ready(function() {
	 	var startInterval;
		var scrollingDiv = jQuery(".scrolld"); 		
		jQuery(window).scroll(function(){			
			scrollingDiv.stop().animate({"marginTop": (jQuery(window).scrollTop() - 200) + "px"}, "slow" );			
		});
	});
    </script>
<?php get_footer(); ?>