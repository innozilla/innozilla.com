<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */
?>
<!doctype html>
<!--[if !IE]>      <html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title><?php wp_title('&#124;', true, 'right'); ?></title>
<link href="<?php bloginfo('template_directory'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>
<meta name="google-site-verification" content="gCOcoLr97yp7t7NCVpJpqBVo5fhoDo1K1AjeQbM3LxQ" />
</head>


	<body <?php body_class(); ?>>


         
    <?php responsive_header(); // before header hook ?>
    <div id="header-inner">

    	<div class="mainhead">
        <div class="topheadbg">
    	<?php if ( get_header_image() != '' ) : ?>
               
        <div id="logo">
            <a href="<?php echo home_url('/'); ?>"><img src="<?php header_image(); ?>" width="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> width;} else { echo HEADER_IMAGE_WIDTH;} ?>" height="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> height;} else { echo HEADER_IMAGE_HEIGHT;} ?>" alt="<?php bloginfo('name'); ?>" /></a>
        </div><!-- end of #logo -->
        
    <?php endif; // header image was removed ?>

    <?php if ( !get_header_image() ) : ?>
                
        <div id="logo">
            <span class="site-name"><a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></span>
            <span class="site-description"><?php bloginfo('description'); ?></span>
        </div><!-- end of #logo -->  

    <?php endif; // header image was removed (again) ?>
<div class="headermenu">
		<?php responsive_header_top(); // before header content hook ?>
    
				<?php wp_nav_menu(array(
				    'container'       => 'div',
						'container_class'	=> 'main-nav',
						'fallback_cb'	  =>  'responsive_fallback_menu',
						'theme_location'  => 'header-menu')
					); 
				?>
                
            <?php if (has_nav_menu('sub-header-menu', 'responsive')) { ?>
	            <?php wp_nav_menu(array(
				    'container'       => '',
					'menu_class'      => 'sub-header-menu',
					'theme_location'  => 'sub-header-menu')
					); 
				?>
            <?php } ?>
        
    <?php responsive_in_header(); // header hook ?>
    
    </div>

<div class="clr"></div>
</div>
			<?php responsive_header_bottom(); // after header content hook ?>
            <!--<div class="header-banner"><img src="<?php //bloginfo('template_directory'); ?>/core/images/header-banner.png" alt=""></div>-->
            <div class="left_form">
               <?php if(!is_page(47)){?>
                <div class="left_formin">
               <?php if (!dynamic_sidebar('headercontactform')) : endif;?>
                </div>
                <?php }?>
           		 <div class="clr"></div>
          </div>
          
          
 		</div>
        
        <!--<div class="sliderbg">
          <div style="margin:0px auto; width:956px; background:#FF0;">asdas</div>
          </div>-->
    </div><!-- end of #header -->
    <?php responsive_header_end(); // after header container hook ?>
                 
<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">

    
    
	<?php responsive_wrapper(); // before wrapper container hook ?>
    <div id="wrapper" class="clearfix">
		<?php responsive_wrapper_top(); // before wrapper content hook ?>
		<?php responsive_in_wrapper(); // wrapper hook ?>