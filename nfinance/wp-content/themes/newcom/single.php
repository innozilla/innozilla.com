<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<?php get_sidebar(); ?>
<div id="content" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">
        <div class="blogdetail">
	<?php //get_template_part( 'loop-header' ); ?>
        
	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>       
				<?php responsive_entry_top(); ?>

                <?php //get_template_part( 'post-meta' ); ?>
				<?php if(get_post_meta($post->ID, "_mcf_removetitle", true)!="yes"){?>
    	            <h1 class="post-title"><?php the_title(); ?></h1>
                 <?php } else {?>
                 <h1 class="post-title">&nbsp;</h1>
                 <?php }?>
                <!--<p class="post-meta"><span class="inner">Posted by <?php //the_author_posts_link(); ?> <?php //the_time('F jS, Y') ?> in <?php //the_category(', ') ?> <?php //comments_popup_link('No Comments', '1 Comment', '% Comments'); ?> <?php //edit_post_link('edit', '| ', ''); ?></span></p>-->

                <div class="post-entry">
                   	<?php /* if ( has_post_thumbnail()) : ?>
                    	<div class="postimg">
							<?php the_post_thumbnail(); ?>
						</div>
					<?php endif;*/ ?>
					<?php the_content(__('Read more &#8250;', 'responsive')); ?>
                    
                    <?php if ( get_the_author_meta('description') != '' ) : ?>
                    
                    <div id="author-meta">
                    <?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '80' ); }?>
                        <div class="about-author"><?php _e('About','responsive'); ?> <?php the_author_posts_link(); ?></div>
                        <p><?php the_author_meta('description') ?></p>
                    </div><!-- end of #author-meta -->
                    
                    <?php endif; // no description, no author's meta ?>
                    
                    <?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
                </div><!-- end of .post-entry -->
                
<?php /*?>                <div class="navigation">
			        <div class="previous"><?php previous_post_link( '&#8249; %link' ); ?></div>
                    <div class="next"><?php next_post_link( '%link &#8250;' ); ?></div>
		        </div><!-- end of .navigation --><?php */?>
                
                <?php //get_template_part( 'post-data' ); ?>
				               
				<?php responsive_entry_bottom(); ?>      
			</div><!-- end of #post-<?php the_ID(); ?> -->       
			<?php responsive_entry_after(); ?>            
            
			<?php //responsive_comments_before(); ?>
			<?php //comments_template( '', true ); ?>
			<?php //responsive_comments_after(); ?>
            
        <?php 
		endwhile; 

		get_template_part( 'loop-nav' ); 

	else : 

		get_template_part( 'loop-no-posts' ); 

	endif; 
	?>  
</div>      
</div><!-- end of #content -->

<?php get_footer(); ?>