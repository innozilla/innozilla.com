<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Main Widget Template
 *
 *
 * @file           sidebar.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/sidebar.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.0
 */

/*
 * If this is a full-width page, exit
 */
if ( 'full-width-page' == responsive_get_layout() ) {
	return;
}
?>

<?php responsive_widgets_before(); // above widgets container hook ?>
<div id="widgets" class="col-xs-12 col-md-4 fit">
	<?php responsive_widgets(); // above widgets hook ?>
		<div class="widget-wrapper-top1">

						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fnewcom.co.il&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>            

                
            
			</div>
            
            <div class="inn-left-container-line"></div>
            
 			<div class="widget-wrapper-top1">
					<?php if (!dynamic_sidebar('homebottomleft')) : endif;?>
                     <div class="bottomtxt1">
                    <?php 
					$catquery = new WP_Query( 'cat=1' );
	                while($catquery->have_posts()) : $catquery->the_post();?>

                        <?php if(get_post_meta($post->ID, "_mcf_homecheck", true)=="yes"){?>
                        	<div class="bottomtxttop">
                              <div class="bottomtximg"><?php if ( has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                    <?php the_post_thumbnail(); ?>
                                        </a>
                                    <?php endif; ?></div>
                              <div class="bottotxt">
                                <p><?php the_content_word(150);?></p>
                                <a href="<?php the_permalink() ?>"rel="bookmark" title="<?php the_title(); ?>">המשך לקרוא >></a> </div>
                              <div class="clr"></div>
                            </div>
						<?php }
						endwhile; ?>
					
					<?php //endif; ?>	
                	</div>
            
			</div>

	<?php responsive_widgets_end(); // after widgets hook ?>
</div><!-- end of #widgets -->
<?php responsive_widgets_after(); // after widgets container hook ?>