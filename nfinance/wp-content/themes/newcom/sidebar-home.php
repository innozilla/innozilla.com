<?php



// Exit if accessed directly

if ( !defined('ABSPATH')) exit;



/**

 * Home Widgets Template

 *

 *

 * @file           sidebar-home.php

 * @package        Responsive 

 * @author         Emil Uzelac 

 * @copyright      2003 - 2013 ThemeID

 * @license        license.txt

 * @version        Release: 1.0

 * @filesource     wp-content/themes/responsive/sidebar-home.php

 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29

 * @since          available since Release 1.0

 */

?>  

	<?php responsive_widgets_before(); // above widgets container hook ?>

   <div id="widgets" class="home-widgets-first">

        <div class="col-xs-12 col-sm-4">

            <div class="widget-wrapper-top1">



						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fnfinance.co.il&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>            



                

            

			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->

        

        <div class="col-xs-12 col-sm-4">

        	<div class="widget-wrapper-top2">

						<?php if (!dynamic_sidebar('hometopmiddel')) : endif;?>

			</div>

        </div><!-- end of .col-300 -->

        

        <div class="col-xs-12 col-sm-4">

            <div class="widget-wrapper-top4">

					<?php if (!dynamic_sidebar('hometopright')) : endif;?>

					<div style="margin-top:15px;">

					<marquee behavior="scroll" direction="up" scrollamount="1" onmouseover="this.stop();" onmouseout="this.start();" style="height:130px;">

					<?php $args = array( 'post_type' => 'testimonials', 'post_status' => 'publish', 'order' => 'DESC', 'offset'=> 0, );

							$loop = new WP_Query( $args );

								if ($loop->have_posts()) : 

									while ( $loop->have_posts() ) : $loop->the_post();?>  

										<h5><?php the_title();?></h5>

										<?php the_content();?>

							<?php endwhile; endif; ?>

							

					</marquee>

					</div>

			</div><!-- end of .widget-wrapper -->



        </div><!-- end of .col-300 -->

         <div class="clr"></div>

    </div>

    <div class="home-saperator"></div>

    

    

    <div class="home-widgets-first">

        <div class="col-xs-12 col-sm-4">

            <div class="widget-wrapper-top1">

					<?php if (!dynamic_sidebar('homebottomleft')) : endif;?>

                     <div class="bottomtxt1">

                    <?php query_posts('cat=1'); ?>

					<?php if (have_posts()) : ?>



						<?php while (have_posts()) : the_post(); ?>

                        <?php if(get_post_meta($post->ID, "_mcf_homecheck", true)=="yes"){?>

                        	<div class="bottomtxttop">

                              <div class="bottomtximg" style = "width:188px !important;"><?php if ( has_post_thumbnail()) : ?>

                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >

                                    <?php the_post_thumbnail('medium_large'); ?>

                                        </a>

                                    <?php endif; ?></div>

                              <div class="bottotxt">

                                <p><?php the_content_word(150);?></p>

                                <a href="<?php the_permalink() ?>"rel="bookmark" title="<?php the_title(); ?>">המשך לקרוא >></a> </div>

                              <div class="clr"></div>

                            </div>

						<?php } endwhile; ?>

					

					<?php endif; ?>	

                	</div>

            

			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->

        

        <div class="col-xs-12 col-sm-4">

        	<div class="widget-wrapper-top3">

				

                    <?php query_posts('cat=3'); ?>

					<?php if (have_posts()) : ?>



						<?php while (have_posts()) : the_post(); ?>

                        <?php if(get_post_meta($post->ID, "_mcf_homecheck", true)=="yes"){?>

                        	<div class="mid_insrd">

                              <div class="mid_insimg"><?php if ( has_post_thumbnail()) : ?>

                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >

                                    <?php the_post_thumbnail(); ?>

                                        </a>

                                    <?php endif; ?></div>

                              <div class="mid_instxt">

                              	<h2><?php the_title();?></h2>

                                <p><?php the_content_word(120);?></p>

                                <a href="<?php the_permalink() ?>"rel="bookmark" title="<?php the_title(); ?>">המשך לקרוא >></a> </div>

                               <div class="clr"></div>

          					</div>

                            <div class="middsap"></div>

						<?php } endwhile; ?>

					

					<?php endif; ?>	                        





			</div>

        </div><!-- end of .col-300 -->

        

        <div class="col-xs-12 col-sm-5 col-md-4">

            <div class="widget-wrapper-top4">



                    <?php query_posts('cat=4&posts_per_page=3'); ?>

					<?php if (have_posts()) : ?>



						<?php while (have_posts()) : the_post(); ?>

                        <?php if(get_post_meta($post->ID, "_mcf_homecheck", true)=="yes"){?>

                        	<div class="mid_insrd">

                              <div class="mid_insimg"><?php if ( has_post_thumbnail()) : ?>

                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >

                                    <?php the_post_thumbnail(); ?>

                                        </a>

                                    <?php endif; ?></div>

                              <div class="mid_instxt">

                              	<h2><?php the_title();?></h2>

                                <p><?php the_content_word(120);?></p>

                                <a href="<?php the_permalink() ?>"rel="bookmark" title="<?php the_title(); ?>">המשך לקרוא >></a> </div>

                               <div class="clr"></div>

          					</div>

                            <div class="middsap"></div>

						<?php } endwhile; ?>

					

					<?php endif; ?>	       



                

            

			</div><!-- end of .widget-wrapper -->



        </div><!-- end of .col-300 -->

         <div class="clr"></div>

    </div>

    

	<?php responsive_widgets_after(); // after widgets container hook ?>