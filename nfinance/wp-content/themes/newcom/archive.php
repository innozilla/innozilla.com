<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;
/**
 * Archive Template
 *
 *
 * @file           archive.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
* @version        Release: 1.1
 * @filesource     wp-content/themes/responsive/archive.php
 * @link           http://codex.wordpress.org/Theme_Development#Archive_.28archive.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
<div id="content" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">

<div class="blogpost">
	<?php if (have_posts()) : ?>

			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
		 
			<?php /* If this is a category archive */ if (is_category()) { ?>				
			<h1 class="post-title">Archive for the '<?php echo single_cat_title(); ?>' Category</h1>
			
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			<h1 class="post-title">Archive for <?php the_time('F jS, Y'); ?></h1>
			
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<h1 class="post-title">Archive for <?php the_time('F, Y'); ?></h1>
	
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<h1 class="post-title">Archive for <?php the_time('Y'); ?></h1>
			
			<?php /* If this is a search */ } elseif (is_search()) { ?>
			<h1 class="post-title">Search Results</h1>
			
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
			<h1 class="post-title">Author Archive</h1>
	
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<h1 class="post-title">Blog Archive</h1>

		<?php } ?>
        
        <?php //get_template_part( 'loop-header' ); ?>
                    
        <?php while (have_posts()) : the_post(); ?>
        
			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>       
				<?php responsive_entry_top(); ?>

                <?php //get_template_part( 'post-meta' ); ?>
                <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
<!--			<p class="post-meta"><span class="inner">Posted by <?php //the_author_posts_link(); ?> <?php //the_time('F jS, Y') ?> in <?php //the_category(', ') ?> <?php //comments_popup_link('No Comments', '1 Comment', '% Comments'); ?> <?php //edit_post_link('edit', '| ', ''); ?></span></p>-->

             
                <div class="post-entry">

					<?php the_excerpt(); ?>
					<?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
				</div><!-- end of .post-entry -->
                
				<div class="clr"></div>
				<?php //get_template_part( 'post-data' ); ?>
				               <p><a class="blogreadmore" href="<?php the_permalink() ?>"><span>המשך לקרוא</span></a></p> 
                               <div class="clr"></div>
				               
				<?php responsive_entry_bottom(); ?>      
			</div><!-- end of #post-<?php the_ID(); ?> -->       

			<?php responsive_entry_after(); ?>
            
        <?php 
		endwhile; 

		get_template_part( 'loop-nav' ); 

	else : 

		get_template_part( 'loop-no-posts' ); 

	endif; 
	?>  
</div>      
</div><!-- end of #content-archive -->
        
<?php get_footer(); ?>