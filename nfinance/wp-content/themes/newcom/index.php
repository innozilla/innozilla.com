<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Index Template
 *
 *
 * @file           index.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/index.php
 * @link           http://codex.wordpress.org/Theme_Development#Index_.28index.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
<div id="content" class="col-xs-12 col-sm-7 col-md-8">
        <h1 class="post-title">Blog</h1>
        <div class="blogpostindex">
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
        
			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>       
				<?php responsive_entry_top(); ?>
				<div class="post-entry blogimg">
				<?php //get_template_part( 'post-meta-page' ); ?>
                <div class="bottomtxttop">
                              <div class="bottomtximg"><?php if ( has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                    <?php the_post_thumbnail(); ?>
                                        </a>
                                    <?php endif; ?></div>
                              <div class="bottotxt">
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink() ?>"rel="bookmark" title="<?php the_title(); ?>">המשך לקרוא >></a> </div>
                              <div class="clr"></div>
                            </div>
				


				</div><!-- end of .post-entry -->
				

				               
				<?php responsive_entry_bottom(); ?>      
			</div><!-- end of #post-<?php the_ID(); ?> -->       
			<?php responsive_entry_after(); ?>
            
			<?php responsive_comments_before(); ?>
			<?php comments_template( '', true ); ?>
			<?php responsive_comments_after(); ?>
            
        <?php 
		endwhile; 

		get_template_part( 'loop-nav' ); 

	else : 

		get_template_part( 'loop-no-posts' ); 

	endif; 
	?>  
      </div>
</div><!-- end of #content -->


<?php get_footer(); ?>