<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/* 
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
?>
		<?php responsive_wrapper_bottom(); // after wrapper content hook ?>
    </div><!-- end of #wrapper -->
    <?php responsive_wrapper_end(); // after wrapper hook ?>
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>
<div class="footermain">
<div id="footer" class="clearfix">
	<?php responsive_footer_top(); ?>

    <div id="footer-wrapper">
    
        <div class="grid col-940">
        	<div class="footerhome">
                        		<?php if (has_nav_menu('footer-menu', 'responsive')) { ?>
									<?php wp_nav_menu(array(
                                            'container'       => '',
                                            'fallback_cb'	  =>  false,
                                            'menu_class'      => 'footer-menu',
                                            'theme_location'  => 'footer-menu')
                                            ); 
                                        ?>
                                 <?php } ?>
        
         	</div><!-- end of col-940 -->
         </div>
        
        

        
    </div><!-- end #footer-wrapper -->
    
	<?php responsive_footer_bottom(); ?>
</div><!-- end #footer -->
</div>
<?php responsive_footer_after(); ?>

<?php wp_footer(); ?>

</body>
<script type="text/javascript">
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      location = 'https://www.nfinance.co.il/תודה-רבה';
  }, false );
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47972018-1', 'nfinance.co.il');
  ga('send', 'pageview');

</script>
</html>