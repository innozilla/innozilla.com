<?php get_header(); ?>

<!-- section one -->
<div class="bg_title innertitle">
	<div class="wrap">
    <h1>שגיאה 404</h1>
    </div>
</div>

<div class="inner_section nofound cf">
	<div class="wrap cf">

        <div class="inner_content">
            <div class="page_con">    
                <h3>מצטערים, לא נמצא</h3>
            </div>
        </div>             

    </div>
</div>

<?php get_footer(); ?>