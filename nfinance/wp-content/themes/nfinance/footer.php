<?php $footer_logo = get_field('footer_logo', 'option');
?>
<div class="footer">
    <div class="wrap cf">
        <?php /*if( have_rows('social_media','option') ): 
			$social_media_title = get_field('social_media_title','option');?>
        <div class="social_icon">
            <h3>
                <?php echo $social_media_title;?>
            </h3>
            <ul class="cf">
                <?php while( have_rows('social_media','option') ): the_row(); 
                    $media_link = get_sub_field('media_link');
                    $media_image = get_sub_field('media_image'); ?>
                <li><a href="<?php echo $media_link;?>" target="_blank"><img src="<?php echo $media_image['url'];?>" alt="<?php echo $media_image['alt'];?>" /></a></li>
                <?php endwhile;?>
            </ul>
        </div>
        <?php endif;*/?>

        <div class="footer_middle cf">

            <div class="footer_col footer_l">
                <?php if($footer_logo!=""){?>
                <div class="footerlogo"><a href="<?php bloginfo('url');?>"><img src="<?php echo $footer_logo['url'];?>" alt="<?php echo $footer_logo['alt'];?>" /></a></div>
                <?php }?>

                <?php if( have_rows('social_media','option') ): 
                    $social_media_title = get_field('social_media_title','option');?>
                <div class="social_icon">

                    <ul class="cf">
                        <?php while( have_rows('social_media','option') ): the_row(); 
                            $media_link = get_sub_field('media_link');
                            $media_image = get_sub_field('media_image'); ?>
                        <li><a href="<?php echo $media_link;?>" target="_blank"><img src="<?php echo $media_image['url'];?>" alt="<?php echo $media_image['alt'];?>" /></a></li>
                        <?php endwhile;?>
                    </ul>
                    <p>
                        <?php echo $social_media_title;?>
                    </p>
                </div>
                <?php endif;?>

            </div>



            <div class="footer_col contact_f">
                <?php $footer_contact_form_title = get_field('footer_contact_form_title','option');
					$footer_contact_form = get_field('footer_contact_form','option');
					$footer_contact_form_bottom_text = get_field('footer_contact_form_bottom_text','option'); ?>
                <h3>
                    <?php echo $footer_contact_form_title;?>
                </h3>
                <div class="footer_form">
                    <?php echo do_shortcode($footer_contact_form);?>
                </div>
                <?php if($footer_contact_form_bottom_text!=""){?>
                <h3 class="conta_bottom">
                    <?php echo $footer_contact_form_bottom_text;?>
                </h3>
                <?php }?>
            </div>
        </div>

        <?php if( have_rows('address_fields_lists','option') ): 
                $address_heading = get_field('address_heading','option');
                ?>
        <div class="contact_detail">
            <?php /*if($address_heading!=""){?>
            <h3>
                <?php echo $address_heading;?>
            </h3>
            <?php }*/?>
            <ul>
                <?php while( have_rows('address_fields_lists','option') ): the_row();
                $label = get_sub_field('label');
                $text = get_sub_field('text');
                $phone = get_sub_field('phone');
                $email = get_sub_field('email');?>
                <li>
                    <?php echo $label;?>
                    <span>
                    <?php echo $text;?>
                    <?php if($phone!=""){?> <a href="tel:<?php echo $phone;?>" target="_blank"><?php echo $phone;?></a><?php }?>
                    <?php if($email!=""){?> <a href="mailto:<?php echo $email;?>" target="_blank"><?php echo $email;?></a><?php }?>
                    </span>
                </li>
                <?php endwhile;?>
            </ul>
        </div>
        <?php endif;?>

        <?php $footer_copyright = get_field('footer_copyright','option');?>
        <div class="copyright">
            <?php echo $footer_copyright;?>
        </div>
    </div>
</div>

</div>
<!-- End container -->

<?php wp_footer(); ?>

<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-47972018-1', 'nfinance.co.il');
    ga('send', 'pageview');

</script>

</body>

</html>