<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="google-site-verification" content="gCOcoLr97yp7t7NCVpJpqBVo5fhoDo1K1AjeQbM3LxQ" />
<title><?php wp_title('&laquo;', true, 'right'); ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico"/>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/fonts/fonts.css" type="text/css" media="screen" />
<?php wp_head(); ?>

<!-- Facebook Pixel Code -->

<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1112347235494200');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=1112347235494200&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager -->


<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NBSHW4');</script>
<!-- End Google Tag Manager -->

<!-- Hotjar Tracking Code for https://www.nfinance.co.il/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:971663,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</head>

<body <?php body_class(); ?>>

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NBSHW4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- Main Container -->
<div class="main_container">


<?php 
$website_logo = get_field('logo', 'option');
$header_contact_from_heading1 = get_field('header_contact_from_heading1','option');
$header_contact_from_heading2 = get_field('header_contact_from_heading2','option');
$header_contact_from_heading3 = get_field('header_contact_from_heading3','option');
$header_contact_from_heading = get_field('header_contact_from_heading','option');
$header_contact_from_shortcode = get_field('header_contact_from_shortcode','option');
$pi = $post->ID;
?>
	<!--  Header -->

    <div class="header cf">
    	<div class="header_bg"></div>
		<div class="wrap cf">
            
            <div class="header_right">

                <div class="header_top cf">
                <?php if($website_logo!='') {?>        
                <div class="logo">
                    <a class="mainlogo" href="<?php bloginfo('url');?>"><img alt="<?php bloginfo('name');?>" src="<?php echo $website_logo['url'];?>" /></a>
                </div>
                <?php } ?>
                        
                        
                <div class="hmenu">
                     <div class="tglmenu nav-icon"><span></span><span></span><span></span></div>  
                     <div class="mobtgl">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'depth' => '4') ); ?>
                    </div>
                </div>
                </div>
                
                
            </div>
            
            <div class="header_left cf">
            	<div class="header_form cf">
					<div class="formheading">
                    <?php //echo $header_contact_from_title;?>
                    <?php if($header_contact_from_heading1!=""){?><p class="slitop1"><?php echo $header_contact_from_heading1;?></p><?php }?>
                    <?php if($header_contact_from_heading2!=""){?><p class="slitop2"><?php echo $header_contact_from_heading2;?></p><?php }?>
                    <?php if($header_contact_from_heading3!=""){?><p class="slitop3"><?php echo $header_contact_from_heading3;?></p><?php }?>
                    <?php if($header_contact_from_heading!=""){?><div class="contacttitle"><?php echo $header_contact_from_heading;?></div><?php }?>
                    </div>
                    <div class="contactheadform cf"><?php echo do_shortcode($header_contact_from_shortcode);?></div>
                </div>
            </div>
        </div>
   </div>        
    <!-- Header End -->