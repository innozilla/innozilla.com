<?php if( have_rows('customers_list','option') ): ?>
<div class="sidebar_customer_re">

	<?php $customers_recommend_description = get_field('customers_recommend_description','option');
	$customers_recommend_title = get_field('customers_recommend_title','option');?>
	<?php if($customers_recommend_title!=""){?>
	<div class="sidebar_bg_title">
		<h3><?php echo $customers_recommend_title;?></h3>
	</div>
	<?php }?>

		<div class="sidebar_recomment_list">
			<?php echo $customers_recommend_description;?>
			<div class="desktop_re">
			<ul class="cf">
				<?php while( have_rows('customers_list','option') ): the_row(); 
					$customer_image = get_sub_field('customer_image');
					$customer_short_description = get_sub_field('customer_short_description');
					$customer_page_link = get_sub_field('customer_page_link');
					$customer_image_thumb = $customer_image['sizes']['medium'];
					?>
					<li>
						<a href="<?php echo $customer_page_link;?>">
							<div class="sidebar_customer_img"><img src="<?php echo $customer_image_thumb;?>" alt="<?php echo $customer_image['alt'];?>" /><span></span></div>
							<?php echo $customer_short_description;?>
						</a>
					</li>
					<?php endwhile;?>
			</ul>
            </div>
            
			<div class="mobile_re">
			<ul class="cf member_slider owl-carousel">
				<?php while( have_rows('customers_list','option') ): the_row(); 
					$customer_image = get_sub_field('customer_image');
					$customer_short_description = get_sub_field('customer_short_description');
					$customer_page_link = get_sub_field('customer_page_link');
					$customer_image_thumb = $customer_image['sizes']['medium'];
					?>
					<li>
						<a href="<?php echo $customer_page_link;?>">
							<div class="sidebar_customer_img"><img src="<?php echo $customer_image_thumb;?>" alt="<?php echo $customer_image['alt'];?>" /><span></span></div>
							<?php echo $customer_short_description;?>
						</a>
					</li>
					<?php endwhile;?>
			</ul>
            </div>            
		</div>
</div>
<?php endif;?>