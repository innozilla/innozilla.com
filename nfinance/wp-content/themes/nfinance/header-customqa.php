<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="google-site-verification" content="gCOcoLr97yp7t7NCVpJpqBVo5fhoDo1K1AjeQbM3LxQ" />
<title><?php wp_title('&laquo;', true, 'right'); ?></title>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico"/>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/fonts/fonts.css" type="text/css" media="screen" />
<?php wp_head(); 


 $pageID = get_the_ID();

if ( $pageID == '958' ) { ?>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '438580166622345');
    fbq('track', 'pageview');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=438580166622345&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google AdWords: 969829902 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-969829902"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-969829902');
    </script>
   

<?php } else { ?>

  <!-- Global site tag (gtag.js) - Google AdWords: 969829902 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-969829902"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-969829902');
    </script>

    <!-- Event snippet for ???? ???? conversion page -->
    <script>
    gtag('event', 'conversion', {'send_to': 'AW-969829902/opNKCOjw_oUBEI7cuc4D'});
    </script>

      <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '438580166622345');
    fbq('track', 'lead');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=438580166622345&ev=PageView&noscript=1"

    /></noscript>
    <!-- End Facebook Pixel Code -->
   
<?php } ?>

</head>

<style type="text/css">
	
.header_form .txtfield textarea,
.footer_form .txtfield textarea {
	width: 100%;
    color: #231f20;
    font-size: 15px;
    height: 30px;
    font-family: 'Assistant-Light';
}

.header_form .txtfield,
.footer_form .txtfield {
    margin-bottom: 15px;
}

</style>

<body <?php body_class(); ?>>
<!-- Main Container -->
<div class="main_container">


<?php 
$website_logo = get_field('logo', 'option');
$header_contact_from_heading1 = get_field('header_contact_from_heading1','option');
$header_contact_from_heading2 = get_field('header_contact_from_heading2','option');
$header_contact_from_heading3 = get_field('header_contact_from_heading3','option');
$header_contact_from_heading = get_field('header_contact_from_heading','option');
$header_contact_from_shortcode = '[contact-form-7 id="962" title="Header Contact custom page"]';
$pi = $post->ID;
?>
	<!--  Header -->

    <div class="header cf">
    	<div class="header_bg"></div>
		<div class="wrap cf">
            
            <div class="header_right">

                <div class="header_top cf">
                <?php if($website_logo!='') {?>        
                <div class="logo">
                    <a class="mainlogo" href="<?php bloginfo('url');?>"><img alt="<?php bloginfo('name');?>" src="<?php echo $website_logo['url'];?>" /></a>
                </div>
                <?php } ?>
                        
                        
                <div class="hmenu">
                     <div class="tglmenu nav-icon"><span></span><span></span><span></span></div>  
                     <div class="mobtgl">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'depth' => '4') ); ?>
                    </div>
                </div>
                </div>
                
                
            </div>
            
            <div class="header_left cf">
            	<div class="header_form cf">
					<div class="formheading">
                    <?php //echo $header_contact_from_title;?>
                    <?php if($header_contact_from_heading1!=""){?><p class="slitop1"><?php echo $header_contact_from_heading1;?></p><?php }?>
                    <?php if($header_contact_from_heading2!=""){?><p class="slitop2"><?php echo $header_contact_from_heading2;?></p><?php }?>
                    <?php if($header_contact_from_heading3!=""){?><p class="slitop3"><?php echo $header_contact_from_heading3;?></p><?php }?>
                    <?php if($header_contact_from_heading!=""){?><div class="contacttitle"><?php echo $header_contact_from_heading;?></div><?php }?>
                    </div>
                    <div class="contactheadform cf"><?php //echo do_shortcode($header_contact_from_shortcode);?>
                    <form method="post" action="http://api.lead.im/v2/submit" id="lm_form" class="wpcf7-form"> 
					<input type="hidden" name="lm_form" value="21215" /> 
					<input type="hidden" name="lm_key" value="c46ea0409f" /> 
					<input type="hidden" name="lm_redirect" value="https://www.nfinance.co.il/%D7%AA%D7%95%D7%93%D7%94-%D7%A8%D7%91%D7%94-cl/" /> 
					<input type="hidden" name="lm_supplier" value="15369" />  
					<div class="txtfield"><input name="name" required type="text" class="wpcf7-validates-as-required" placeholder="שם" maxlength="300" /></div>
					<div class="txtfield"><input name="lname" required type="text" placeholder="שם משפחה" maxlength="300" /></div>
					<div class="txtfield"><input name="phone" required type="text" placeholder="טלפון" maxlength="300" /></div>
					<div class="txtfield"><input name="email" required type="text" placeholder="דוא&quot;ל" maxlength="300" /></div>
					<div class="txtfield"><textarea name="msg" type="text" placeholder="הערות" maxlength="3500"></textarea></div>
					<div class="hecontbutton"><input type="submit" class="" value="שלח/י"/></div>
					</form>
					</div>

                </div>
            </div>
        </div>
   </div>        
    <!-- Header End -->
    

 