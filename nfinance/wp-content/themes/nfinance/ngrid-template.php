<?php 

// Template Name: ngrid

get_header(); ?>


<style type="text/css">
.ngrid-main-wrap {
	padding: 80px 0px;
}
.ngrid-main-wrap p {
	font-size: 16px;
	padding-bottom: 20px;
}
.ngrid-main-wrap .img-wp {
	margin-bottom: 25px;
	float: right;
}
.ngrid-main-wrap .img-wp img {
	border: 1px solid #efefef;
	padding: 5px;
	box-shadow: 1px 1px 10px #eaeaea;
}
.hero {
	padding: 200px 0px;
	text-align: center;
	background-size: cover;
	margin-top: -15px;
	position: relative;
	z-index: 9999;
}
.hero h1 {
	color: #fff;
}

</style>

<!-- <div class="hero" style="background-image:url('https://www.hon-hozer.co.il/wp-content/uploads/2017/04/thanks.jpg');">
	<div class="container-fluid">
	  <div class="row">
	      <div class="col-xs-12">
	        <h1>Lorem Ipsum Dolor</h1>
	      </div>
	  </div>
	</div>
</div> -->
<div class="ngrid-main-wrap">
  <div class="container">
    <div class="row">

      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z1.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z2.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z3.jpg">
      </div>

    </div>  
    <div class="row">
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z4.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z5.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z6.jpg">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z7.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z8.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z10.jpg">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z9.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z11.jpg">
      </div>
      <div class="col-md-4 img-wp">
          <img src="https://www.nfinance.co.il/wp-content/uploads/2019/02/z12.jpg">
      </div>
    </div>



  </div>
</div>


<?php get_footer(); ?>