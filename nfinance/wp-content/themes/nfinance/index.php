<?php /*Blog Page*/
get_header(); 
?>

<!-- section one -->
<div class="bg_title innertitle">
	<div class="wrap">
    <h1>בלוג</h1>
    </div>
</div>

<!-- section one -->
<div class="inner_section blog_m cf">
    
    	<div class="blog_list">
        <div class="wrap">
                <?php //query_posts($query_string . '&order=ASC'); // FOR ASCENDING ORDER	
                //$args = array( 'post_type' => 'post', 'post_status' => 'publish', 'order' => 'DESC', 'offset'=> 0, 'posts_per_page' => 6);
               // $loop = new WP_Query($args);
                if (have_posts()) :?>
                <ul class="cf">
                <?php $cat=1; while (have_posts()) : the_post(); 
				$feat_image=wp_get_attachment_url( get_post_thumbnail_id(get_the_id()));
            	$image_id = get_post_thumbnail_id(get_the_id());
   				$thumb = wp_get_attachment_image_src($image_id,'thumbnail', true);
   				$show_img=$thumb['0'];		
                ?>
               
                <li class="cf">
                <a href="<?php the_permalink(); ?>" class="cf">
                	<div class="blog_thumb"><img src="<?php if($feat_image!=""){echo $show_img;} else { echo bloginfo('template_directory')."/images/dafult_logo.jpg";} ?>" alt="<?php the_title(); ?>" /></div> 
                    <div class="blog_left">
                    	<h4><?php the_title(); ?></h4>
                    	<div class="blog_more">קרא עוד</div>     
                    </div>          
                </a>
                </li>

                <?php $cat++; endwhile;?>
                </ul>
                <?php endif; ?>
                
			<div class="navigation">
				<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
		
		        <div class="alignleft"><?php next_posts_link('&larr; Previous Entries') ?></div>
		        <div class="alignright"><?php previous_posts_link('Next Entries &rarr;') ?></div>
		        <?php } ?>
			</div>                
        </div>            
    	</div> 
       

</div>    


<?php get_footer(); ?>