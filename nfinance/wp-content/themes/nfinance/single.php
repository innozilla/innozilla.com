<?php
get_header(); 
$pid=$post->ID; 
?>

<!-- section one -->
<div class="bg_title innertitle">
	<div class="wrap">
    <h1><?php echo the_title();?></h1>
    </div>
</div>

<!-- section one -->
<div class="inner_section cf">
	<div class="wrap cf">
        <div class="inner_a_right">
                <div class="inner_content">
                    <div class="page_con">    
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
                        <?php the_content(); ?>
                    <?php endwhile; endif; ?>
                    </div>
                </div>             
        </div>
        
        <div class="inner_a_left">
            <?php include('customer-sidebar.php');?>
        </div>        
    </div>
</div>
<!-- section one -->


<?php get_footer(); ?>