<?php
/** Template Name: Q & A Page */ 
get_header(); 
$pid=$post->ID; 
?>
<style type="text/css">
.question p {
   color: #000;
   font-size: 16px;
}
.blink_me {
   -webkit-animation-name: blinker;
   -webkit-animation-duration: 1s;
   -webkit-animation-timing-function: linear;
   -webkit-animation-iteration-count: infinite;
   -moz-animation-name: blinker;
   -moz-animation-duration: 1s;
   -moz-animation-timing-function: linear;
   -moz-animation-iteration-count: infinite;
   animation-name: blinker;
   animation-duration: 1s;
   animation-timing-function: linear;
   animation-iteration-count: infinite;
}
 @-moz-keyframes blinker {
 0% {
opacity: 1.0;
}
 50% {
opacity: 0.0;
}
 100% {
opacity: 1.0;
}
}
 @-webkit-keyframes blinker {
 0% {
opacity: 1.0;
}
 50% {
opacity: 0.0;
}
 100% {
opacity: 1.0;
}
}
 @keyframes blinker {
 0% {
opacity: 1.0;
}
 50% {
opacity: 0.0;
}
 100% {
opacity: 1.0;
}
}
</style>
<!---------Guage Edit-->
<?php $h_titlte = get_field('hebrew_title', $pid);
$h_marks = get_field('hebrew_marks', $pid);
$tax_refund = get_field('tax_refound', $pid);
?>
<script src="<?php bloginfo('template_url'); ?>/justgage/js/raphael.2.1.0.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/justgage/js/justgage.1.0.1.min.js"></script>
 
  
  <!-- section one -->
  <div class="bg_title innertitle">
    <div class="wrap">
      <h1><?php echo the_title();?></h1>
    </div>
  </div>
  
  <!-- section one -->
  <div class="inner_section cf hfeed">
    <div class="wrap cf">
    <div id="qanda">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      
      <div class="inner_a_right">
      <div id="content" class="col-620">
        <div class="inner_content">
          <div class="page_con post-entry">

            <?php //the_content(); ?>
            
            
            <?php if( have_rows('q_a_lists',$pid) ): ?>
            <div id="Ques" class="Answer">
              <div class="test">
            <?php $cat=1;
                while( have_rows('q_a_lists',$pid) ): the_row();
                $question = get_sub_field('question');
                $answers = get_sub_field('answers');              
                ?>
                <div class="test_inner">
                  <div class="right"><span class="ques_no"><?php echo $cat;?></span></div>
                  <div class="question"> <?php echo $question;?>
                     <?php if( have_rows('answers',$pid) ): ?>
                    <ul class="options">
                        <?php $opt=1;
                  while( have_rows('answers',$pid) ): the_row();
                  $radio_label = get_sub_field('radio_label');
                  $radio_value = get_sub_field('radio_value');             
                  ?>
                    
                      <li>
                        <input id="choicen<?php echo $opt.$cat;?>" name="choice<?php echo $cat;?>" type="radio" value="<?php echo $radio_value;?>" />
                        <label for="choicen<?php echo $opt.$cat;?>"><?php echo $radio_label;?></label>
                      </li>
                      <?php $opt++; endwhile;?>

                    </ul>
                    <?php endif;?>
                  </div>
                </div>
               <?php $cat++; endwhile;?>
              </div>
            </div>
            <?php endif;?>
            
            
            <?php $form_heading = get_field('form_heading',$pid);
         $bottom_form_shortcode = get_field('bottom_form_shortcode',$pid);
         $bottom_image = get_field('bottom_image',$pid); ?>
            <div class="qa_form desktop">
                <?php if($form_heading!=""){?><h3><?php echo $form_heading;?></h3><?php }?>
                <?php if($bottom_image!=""){?><img src="<?php echo $bottom_image['url'];?>" alt="<?php echo $bottom_image['alt'];?>" class="alignnone size-full wp-image-674" /><?php }?>
                <div id="themify_builder_content-505" data-postid="505" class="themify_builder_content themify_builder_content-505 themify_builder themify_builder_front ui-sortable">
   
   
</div>
                <?php if($bottom_form_shortcode!=""){?><div class="c_form col-full"> <?php echo do_shortcode($bottom_form_shortcode); ?> </div><?php }?>
            </div>
          </div>
        </div>
      </div>
      </div>
      <div class="inner_a_left grid col-330-1 fit scrolld" id="widgets">&nbsp;
          <div id="g1" style="width:500px; height:300px; color:#000;"></div>
      </div>
      <div class="qa_form mobile">
                <?php if($form_heading!=""){?><h3><?php echo $form_heading;?></h3><?php }?>
                <?php if($bottom_image!=""){?><img src="<?php echo $bottom_image['url'];?>" alt="<?php echo $bottom_image['alt'];?>" class="alignnone size-full wp-image-674" /><?php }?>
                <div id="themify_builder_content-505" data-postid="505" class="themify_builder_content themify_builder_content-505 themify_builder themify_builder_front ui-sortable">
   
   
         </div>
          <?php if($bottom_form_shortcode!=""){?><div class="c_form col-full"> <?php echo do_shortcode($bottom_form_shortcode); ?> </div><?php }?>
      </div>
      <?php endwhile; endif; ?>
    </div>
  </div>
</div>
<!-- end of #content --> 
<script>
      var g1;
     
   function calcscore(){
      var score = 0;
      //jQuery(".options li").removeClass("bg_green");
      jQuery("input[type=radio]:checked").each(function(){
      
      //jQuery(this).parent().addClass("bg_green");
      if(jQuery(this).val() == 999){ //alert(jQuery(this).val());
      score= 0;
      }else{
        score+=parseInt(jQuery(this).val(),10); 
      }
    });
    return score;
   }
     
     jQuery("input[type=radio]").change(function(){ 
        var x = calcscore(); //alert(x);
        calcload(x);
          
      });
     function calcload(val){
      
         //alert(val);
        if(val >= 50){
        jQuery("#g1").html("");
          var g1 = new JustGage({
             id: "g1", 
             value: val,
          min: 0,
          max: 100,
          title: '<?php echo $tax_refund; ?>',
          label: '<?php echo $h_marks; ?>',
        titleFontColor: "green",
        });
        //jQuery("#g1 tspan").fadeOut('fast');
        //jQuery("#g1 tspan").slideUp( 300 ).delay(300).slideDown( 400 );
        //delayFunction();
        startInterval = setInterval(delayFunction, 100);
        }else{
        
        jQuery("#g1").html("");
          var g1 = new JustGage({
             id: "g1", 
             value: val,
          min: 0,
          max: 100,
          title: '<?php echo $h_titlte; ?>',
          label: '<?php echo $h_marks; ?>'
        });
        
        clearInterval(startInterval);
        
        }
      
      }
      
     function delayFunction() {
      jQuery("#g1 tspan").first().slideUp( 300 ).delay(300).slideDown( 400 );
      }
     window.onload = function(){ 
        var g1 = new JustGage({
          id: "g1", 
          value: 0,
          min: 0,
          max: 100,
           title: '<?php echo $h_titlte; ?>',
          label: '<?php echo $h_marks; ?>'
        });
      };
    jQuery(document).ready(function() {
      var startInterval;
      //var scrollingDiv = jQuery(".scrolld");     
      //jQuery(window).scroll(function(){       
      // scrollingDiv.stop().animate({"marginTop": (jQuery(window).scrollTop() - 100) + "px"}, "slow" );       
      //});
      
      var $scrollingDiv = $(".scrolld");
      $(window).scroll(function(){            
      var y = $(this).scrollTop(),
         maxY = $('.c_form').offset().top,
         scrollHeight = $scrollingDiv.height();
      if(y< maxY-scrollHeight ){
         $scrollingDiv
         .stop()
         .animate({"marginTop": ($(window).scrollTop()) + "px"}, "slow" );        
      }    
      });
      
   });
    </script>
<?php get_footer(); ?>
