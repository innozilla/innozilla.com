<?php /*Template Name: Contact Page */
get_header();
$pid=$post->ID; 
?>
<!-- section one -->
<div class="bg_title innertitle">
	<div class="wrap">
    <h1><?php echo the_title();?></h1>
    </div>
</div>

<!-- section one -->
<div class="inner_section cf">
	<div class="wrap cf">
        <div class="inner_a_right">
                <div class="inner_content">
                    <div class="page_con cf">    
					<?php if( have_rows('address_fields_lists','option') ): 
                    $address_heading = get_field('address_heading','option');
                    ?>
                    <div class="contactformright"> 
                    <?php if($address_heading!=""){?><h3><?php echo $address_heading;?></h3><?php }?>
                    <ul>
                    <?php while( have_rows('address_fields_lists','option') ): the_row();
                    $label = get_sub_field('label');
                    $text = get_sub_field('text');
                    $phone = get_sub_field('phone');
                    $email = get_sub_field('email');?>
                    <li><?php echo $label;?> 
                        <span>
                        <?php echo $text;?>
                        <?php if($phone!=""){?> <a href="tel:<?php echo $phone;?>" target="_blank"><?php echo $phone;?></a><?php }?>
                        <?php if($email!=""){?> <a href="mailto:<?php echo $email;?>" target="_blank"><?php echo $email;?></a><?php }?>
                        </span>
                    </li>
                    <?php endwhile;?>
                    </ul>
                    </div>
                    <?php endif;?>   


                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
                    <div class="contactformleft"><?php the_content(); ?></div>
                    <?php endwhile; endif; ?>
                    </div>
                </div>             
        </div>

        <div class="inner_a_left">
            <?php include('customer-sidebar.php');?>
        </div>        
    </div>
</div>


<?php get_footer(); ?>