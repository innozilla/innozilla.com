<?php /*Template Name: Services Page */
get_header();
$pid=$post->ID;

$service_left_image = get_field('service_left_image',$pid);
?>


<!-- section one -->
<div class="service_section cf">	
	
    	<div class="desktopsec">
            <div class="service_cont" style="background:url('<?php echo $service_left_image['url'];?>') no-repeat; background-size:cover;">
                <?php //if (have_posts()) : while (have_posts()) : the_post(); ?>	
                    <div class="service_txt" >
                    </div>
                    
                <?php //endwhile; endif; ?>
            </div>
            <div class="service_list">
                <div class="bradcrumbs postition"><a href="<?php bloginfo('url');?>">Home</a> <?php if(function_exists('bcn_display')) { bcn_display(); }?></div>
                <div class="service_slide owl-carousel">
                        <?php query_posts($query_string . '&order=DESC'); 
                        $count = 1;
                        $args = array( 'post_type' => 'service', 'post_status' => 'publish', 'order' => 'DESC', 'offset'=> 0, 'posts_per_page' => -1);
                        $loop = new WP_Query( $args );
                        if ($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();
                        $service_icon = get_field('service_icon');
                        $short_descriptoin = get_field('short_descriptoin'); ?> 
                        <div class="service_item">
                        	<a class="topspace" href="<?php the_permalink() ?>">
                                <div >   
                                    <div class="serviceimg">
                                        <img src="<?php echo $service_icon['url']; ?>" alt="<?php echo $service_icon['alt']; ?>" />
                                    </div>
                                    <div class="slide_ser">
                                        <div class="wrap_sli">
                                            <h3><?php the_title();?></h3>
                                            <?php echo $short_descriptoin; ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
							<a href="<?php the_permalink() ?>">
                                <span class="redmore"><img src="<?php bloginfo('template_directory'); ?>/images/red-arrow-Services.svg" alt="" /></span>
                            </a>                        
                        </div>
                        <?php $count++; endwhile; endif; ?>
                    
                </div>  
         
            </div>
    	</div>
        
    	<div class="mobilesec">
            <div class="service_list">
                <div class="bradcrumbs postition"><a href="<?php bloginfo('url');?>">Home</a> <?php if(function_exists('bcn_display')) { bcn_display(); }?></div>
                <div class="service_slide_mob owl-carousel">
                		<div class="service_item service_cont" style="background:url('<?php echo $service_left_image['url'];?>') no-repeat; background-size:cover;">
							<?php //if (have_posts()) : while (have_posts()) : the_post(); ?>	
                                <div class="service_txt" >
                                    <div class="latest_pro_awap">
                                    <img src="<?php bloginfo('template_directory'); ?>/images/Swipe-icon-for-mobile.svg" alt="Swipe" />
                                    <span>Swipe For Enjoy Services</span>
                                    </div>
                                </div>
                                
                            <?php //endwhile; endif; ?>
                        </div>
                        <?php query_posts($query_string . '&order=DESC'); 
                        $count = 1;
                        $args = array( 'post_type' => 'service', 'post_status' => 'publish', 'order' => 'DESC', 'offset'=> 0, 'posts_per_page' => -1);
                        $loop = new WP_Query( $args );
                        if ($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post();
                        $service_icon = get_field('service_icon');
                        $short_descriptoin = get_field('short_descriptoin'); ?> 
                        <div class="service_item">
                        	<a class="topspace" href="<?php the_permalink() ?>">
                                <div>   
                                    <div class="serviceimg">
                                        <img src="<?php echo $service_icon['url']; ?>" alt="<?php echo $service_icon['alt']; ?>" />
                                    </div>
                                    <div class="slide_ser">
                                        <div class="wrap_sli">
                                            <h3><?php the_title();?></h3>
                                            <?php echo $short_descriptoin; ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            
                            <a href="<?php the_permalink() ?>">   
                                <span class="redmore"><img src="<?php bloginfo('template_directory'); ?>/images/red-arrow-Services.svg" alt="" /></span>
                            </a>                        
                        </div>
                        <?php $count++; endwhile; endif; ?>
                    
                </div>  
         
            </div>
    	</div>        
</div>
<!-- section one -->


<?php get_footer(); ?>