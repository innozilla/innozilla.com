<?php
/** Template Name: Global Thank You Page*/ 
get_header('customqa'); 
$pid=$post->ID; 
?>
<style type="text/css">


.ty_wrap {
    max-width: 1200px;
    margin: 0px auto;
    position: relative;
    padding: 100px 30px 0px;
}

.ty_wrap h2 {
    font-size: 42px;
    margin: 0px;
    line-height: 45px;
    padding: 0px 0px 20px 0px;
    font-family: 'Assistant-SemiBold', sans-serif;
}

.ty_wrap h5 a {
    color: #e51c43;
    font-size:23px;
}

@media only screen and (max-width: 850px) { 
    .ty_wrap {
        padding: 50px 30px;
    }

    .ty_wrap h2 {
      font-size: 30px;
    }
}

</style>

 <div class="ty_wrap">

    <h2>תודה רבה נחזור אליכם בהקדם   </h2>
    <h5><a href="/"> לחץ כאן בכדי לחזור לדף</a></h5>


 </div>

 <?php get_footer('customqa'); ?>