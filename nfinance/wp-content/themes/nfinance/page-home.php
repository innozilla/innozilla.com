<?php /*Template Name: Home Page */
include('header_home.php');
$pid=$post->ID;
$header_bg_image = get_field('header_bg_image',$pid);

?>


<div class="container">
	<!------------ EXPERTIZE------------->
	<?php if( have_rows('expertise_list',$pid) ): 
	$expertise_top_heading = get_field('expertise_top_heading',$pid);
	$expertise_heading = get_field('expertise_heading',$pid);?>
    <div class="our_expertise">
    	<div class="wrap">
        	<?php if($expertise_top_heading!=""){?><h2><?php echo $expertise_top_heading;?></h2><?php }?>
    		<?php if($expertise_heading!=""){?><h3><?php echo $expertise_heading;?></h3><?php }?>
        	<ul class="cf">              
                    <?php while( have_rows('expertise_list',$pid) ): the_row(); 
                        $expertise_image = get_sub_field('expertise_image');
						$expertise_title = get_sub_field('expertise_title');
						$expertise_link = get_sub_field('expertise_link');
						$expertise_short_text = get_sub_field('expertise_short_text');?>
                
                            <li class="cf">
                                <a href="<?php echo $expertise_link;?>">
                                    <img src="<?php echo $expertise_image['url'];?>" alt="<?php echo $expertise_image['alt'];?>" />
                                    <h3><span><?php echo $expertise_title;?></span></h3>
                                    <?php echo $expertise_short_text;?>
                                </a>
                            </li>              
                    <?php endwhile; ?>  
            </ul>
        </div>
    </div>
    <?php endif; ?>
    
    <!------------ MIDDLE------------->
    
    <?php $middle_image = get_field('middle_image',$pid);
	$middle_heading = get_field('middle_heading',$pid);
	$middle_sub_heading = get_field('middle_sub_heading',$pid);
	$middle_description = get_field('middle_description',$pid);?>
    
    <div class="middle_area">
    	<div class="middle_top_border"><img src="<?php bloginfo('template_directory'); ?>/images/middle_top_border.png" alt="" /></div>
        	<div class="mdiddle_content cf">
                    <div class="middle_left" style="background:url(<?php if($middle_image!=""){ echo $middle_image['url'];} else {bloginfo('template_directory'); ?>/images/middle_img.jpg<?php }?>) no-repeat top right; background-size:cover;"><div class="middleim"><img src="<?php if($middle_image!=""){ echo $middle_image['url'];} else {bloginfo('template_directory'); ?>/images/middle_img.jpg<?php }?>" alt="" /></div></div>
                    <div class="middle_right">
                    	<div class="middle_inner cf">
                        <?php if($middle_heading!=""){?><h2><?php echo $middle_heading;?></h2><?php }?>
                        <?php if($middle_sub_heading!=""){?><h3><?php echo $middle_sub_heading;?></h3><?php }?>
                        
                        <?php echo $middle_description;?>                        
                        <?php /*if($middle_button_link!=""){?><div class="rmore"><a href="<?php echo $middle_button_link;?>">קרא עוד</a></div><?php }*/?>
                        </div>
                    </div>
            </div>
        <div class="middle_bottom_border">
        	<img src="<?php bloginfo('template_directory'); ?>/images/middle_bottom_border_new.png" alt="" />
            <div class="blog_title">
                <div class="wrap">
                <?php $extension_title = get_field('extension_title',$pid);?>
                    <h3><?php echo $extension_title;?></h3>
                </div>
            </div>
        </div>
    </div>
    
    <!------------ EXTENSION------------->
	<div class="extension">
    	<div class="wrap">
        	<?php if( have_rows('extension_list',$pid) ): ?>
			<ul class="cf">    
				<?php while( have_rows('extension_list',$pid) ): the_row(); 
                    $icon = get_sub_field('icon');
                    $short_text = get_sub_field('short_text');
					$page_link = get_sub_field('page_link');?>
                    <li>
                        <a href="<?php echo $page_link;?>" class="cf">
                            <div class="extension_icon"><img src="<?php echo $icon['url'];?>" alt="<?php echo $icon['alt'];?>" /></div>
                            <div class="extension_txt"><div class="extension_sort"><span><?php echo $short_text;?></span></div></div>
                        </a>
                    </li>                         
                <?php endwhile; ?>  
			</ul>
			<?php endif; ?>

        </div>
    </div>
    
    <!------------ REPORT------------->
    
    <?php $report_image = get_field('report_image',$pid);
	$report_heading = get_field('report_heading',$pid);
	$report_description = get_field('report_description',$pid);
	$report_bottom_text = get_field('report_bottom_text',$pid);?>
    
    <div class="report_area">
    	<div class="report_top_border"><img src="<?php bloginfo('template_directory'); ?>/images/middle_top_border.png" alt="" /></div>
        	<div class="report_content cf">
                    
                    <div class="report_left" style="background:url(<?php if($report_image!=""){ echo $report_image['url'];} else {bloginfo('template_directory'); ?>/images/report_img.jpg<?php }?>) no-repeat top right; background-size:cover;"><div class="middleim"><img src="<?php if($middle_image!=""){ echo $middle_image['url'];} else {bloginfo('template_directory'); ?>/images/middle_img.jpg<?php }?>" alt="" /></div></div>
                    <div class="report_right">
                    	<div class="report_inner cf">
                        <?php if($report_heading!=""){?><h2><?php echo $report_heading;?></h2><?php }?>
                        <?php echo $report_description;?>
                        
                        <?php if($report_bottom_text!=""){?><p class="bottxt"><?php echo $report_bottom_text;?></p><?php }?>                    
                        </div>
                    </div>
            </div>
        <div class="middle_bottom_border">
        	<img src="<?php bloginfo('template_directory'); ?>/images/middle_top_bottom_report.png" alt="" />
            
        </div>
    </div>
    
    
    
    <!------------ BLOG------------->
    <div class="blog_list">
        <div class="wrap">
        

                <?php //query_posts($query_string . '&order=ASC'); // FOR ASCENDING ORDER	
                $args = array( 'post_type' => 'post', 'post_status' => 'publish', 'order' => 'DESC','orderby' => 'menu-order' , 'meta_query' => array(array('key' => 'display_in_home','value' => 'yes','compare' => 'LIKE')), 'offset'=> 0, 'posts_per_page' => -1);
                $loop = new WP_Query($args);
                if ($loop->have_posts()) :?>
                <ul class="cf">
                <?php $cat=1; while ( $loop->have_posts() ) : $loop->the_post();
				$feat_image=wp_get_attachment_url( get_post_thumbnail_id(get_the_id()));
            	$image_id = get_post_thumbnail_id(get_the_id());
   				$thumb = wp_get_attachment_image_src($image_id,'thumbnail', true);
   				$show_img=$thumb['0'];		
                ?>
                <?php if($cat==3){
					//$cat=0;?>
					</ul><div class="break_bo"><img src="<?php echo bloginfo('template_directory')."/images/blog_border.jpg"; ?>" alt="" /></div><ul class="cf">
				<?php  $cat=1;}?>                
                <li class="cf">
                <a href="<?php the_permalink(); ?>" class="cf">
                	<div class="blog_thumb"><img src="<?php if($feat_image!=""){echo $show_img;} else { echo bloginfo('template_directory')."/images/dafult_logo.jpg";} ?>" alt="<?php the_title(); ?>" /></div> 
                    <div class="blog_left">
                    	<h4><?php the_title(); ?></h4>
                    	<div class="blog_more">קרא עוד</div>     
                    </div>          
                </a>
                </li>

                <?php $cat++; endwhile;?>
                </ul>
                <?php endif; ?>
        </div>            
    </div>


	<!------------ CUSTOMERS------------->
	<?php if( have_rows('customers_list','option') ): ?>
	<div class="customer_re">
    
    	<?php $customers_recommend_description = get_field('customers_recommend_description','option');
		$customers_recommend_title = get_field('customers_recommend_title','option');
		$customers_red_recommend_heading = get_field('customers_red_recommend_heading','option');?>
    	<?php if($customers_recommend_title!=""){?>
        <div class="bg_title">
        	<h3><?php echo $customers_recommend_title;?></h3>
        </div>
        <?php }?>
        
        <div class="wrap">
        	<div class="recomment_list">
            	
            	<?php echo $customers_recommend_description;?>
                <h3><?php echo $customers_red_recommend_heading;?></h3>
                <div class="desktop_re">
                <ul class="cf member_slider_desk owl-carousel">
                	<?php while( have_rows('customers_list','option') ): the_row(); 
                        $customer_image = get_sub_field('customer_image');
						$customer_short_description = get_sub_field('customer_short_description');
						$customer_page_link = get_sub_field('customer_page_link');
						$customer_image_thumb = $customer_image['sizes']['medium'];
						?>
                        <li>
                        	<a href="<?php echo $customer_page_link;?>">
                            	<div class="customer_img"><img src="<?php echo $customer_image_thumb;?>" alt="<?php echo $customer_image['alt'];?>" /><span></span></div>
                                <?php echo $customer_short_description;?>
                            </a>
                        </li>
                        <?php endwhile;?>
                </ul>
                </div>
                
                <div class="mobile_re">
                <ul class="cf member_slider owl-carousel">
                	<?php while( have_rows('customers_list','option') ): the_row(); 
                        $customer_image = get_sub_field('customer_image');
						$customer_short_description = get_sub_field('customer_short_description');
						$customer_page_link = get_sub_field('customer_page_link');
						$customer_image_thumb = $customer_image['sizes']['medium'];
						?>
                        <li>
                        	<a href="<?php echo $customer_page_link;?>">
                            	<div class="customer_img"><img src="<?php echo $customer_image_thumb;?>" alt="<?php echo $customer_image['alt'];?>" /><span></span></div>
                                <?php echo $customer_short_description;?>
                            </a>
                        </li>
                        <?php endwhile;?>
                </ul>
                </div>                
            </div>
        </div>
    </div>
    <?php endif;?>
    
<?php get_footer(); ?>