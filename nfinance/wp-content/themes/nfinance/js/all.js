jQuery(document).ready(function($){	
    $('.header_slider').owlCarousel({
        items : 1,
        nav: false,
        autoplay: true,
        dots: true,
        slideSpeed : 1000,
        smartSpeed: 200,
        loop: true,
		rtl: true,
        animateOut: 'fadeOut',
        navText: ["",""],
    });
	
    $('.member_slider').owlCarousel({
        items : 1,
        nav: false,
        autoplay: true,
        dots: true,
        slideSpeed : 1000,
        smartSpeed: 200,
        loop: true,
		rtl: true,
        animateOut: 'fadeOut',
        navText: ["",""],
    });
	
    $('.member_slider_desk').owlCarousel({
        items : 3,
        nav: false,
        autoplay: false,
        dots: true,
        slideSpeed : 1000,
        smartSpeed: 200,
        loop: true,
		rtl: true,
        animateOut: 'fadeOut',
        navText: ["",""],
    });	
		
	$(".tglmenu").click(function(){
		$('.mobtgl').slideToggle(400);
		//$(this).toggleClass('open');
	});	
	//$(".hmenu ul li a").click(function(){
		//$('.mobtgl').slideUp(400);	
		//$('.tglmenu').removeClass('open');
	//});
	
	$(".tipsmenu a").click(function(e){
		e.preventDefault();
		$('.tabs').hide();
		$('.tipsmenu a').removeClass('active');
		$(this.hash).fadeIn(100);
		$(this).addClass('active');		
	}).filter(':first').click();
	
	$(".tipsmenumob a").click(function(e){
		e.preventDefault();
		$('.tabsm').hide();
		$('.tipsmenumob a').removeClass('active');
		$('html,body').animate({scrollTop: $('.tab_content.moc').offset().top-77}, 1000, 'swing'); 
		$(this.hash).fadeIn(100);
		$(this).addClass('active');		
	}).filter(':first').click();	


    $('.sponsor_slider').owlCarousel({
        items : 4,
        nav: false,
        autoplay: false,
        dots: true,
        slideSpeed : 1000,
        smartSpeed: 200,
        loop: true,
		margin:0,
		rtl:false,
        animateOut: 'fadeOut',
       // navText: ["",""],
	   responsive:{0:{items:2}, 400:{items:2,}, 768:{items:3,}, 981:{items:4,}},	
    });	
	
	//$(window).on('scroll',function(){
	//	$(".header").toggleClass("sticky", $(document).scrollTop() >= 50);
	//});	
	
});

