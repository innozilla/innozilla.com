<?php /*Template Name: About us*/
get_header(); 
$pid=$post->ID; 
$about_left_image = get_field('about_left_image',$pid);?>


<!-- section one -->
<div class="inner_section cf">
    <div class="inner_a_left" style="background:url('<?php echo $about_left_image['url'];?>') no-repeat; background-size:cover;"></div>
    <div class="inner_a_right">
            <div class="inner_content_about">
                <div class="bradcrumbs"><a href="<?php bloginfo('url');?>">Home</a> <?php if(function_exists('bcn_display')) { bcn_display(); }?></div>
                <h1><?php the_title();?></h1>
                <div class="about_con">    
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
                    <?php the_content(); ?>
                <?php endwhile; endif; ?>
                </div>
            </div>
            
            <div class="ourpartner">
            	<div class="ourpart_wrap">
                	<?php $partner_heading = get_field('partner_heading',$pid);
					$partner_description = get_field('partner_description',$pid);?>
                	<?php if($partner_heading!=""){?><h3><?php echo $partner_heading;?></h3><?php }?>
                    <?php if($partner_description!=""){?><p><?php echo $partner_description;?></p><?php }?>
                    <?php if( have_rows('partners_list',$pid) ): ?>

                    <ul class="cf">                
                    <?php while( have_rows('partners_list',$pid) ): the_row(); 
                        $partner_name = get_sub_field('partner_name');
                        $partner_link = get_sub_field('partner_link');?>
                		<li><a href="<?php echo $partner_link;?>" target="_blank"><?php echo $partner_name;?></a></li>                
                    <?php endwhile; ?>                
                    </ul>                
               		<?php endif; ?>  
                </div>
            </div>
            
            
            <div class="atmosphere">
            	<?php $atmosphere_title = get_field('atmosphere_title',$pid);?>
            	<img src="<?php bloginfo('template_directory'); ?>/images/atmosphere.jpg" alt="" />
                
                <?php /*?><ul class="cf">
                	<li class="width60"><?php if($atmosphere_title!=""){?><h3><?php echo $atmosphere_title;?></h3><?php }?></li>
                    <li class="width20"></li>
                    <li class="width20"></li>
                    <li class="width60"></li> 
                </ul><?php */?>
            </div>
            
            
            <?php $about_contact_form = get_field('about_contact_form','option');			
			$contact_title = get_field('contact_title','option');
			
			$email_address_title = get_field('email_address_title','option');
			$email_address = get_field('email_address','option');
			
			$telephone_title = get_field('telephone_title','option');
			$telephone = get_field('telephone','option');
			
			$address_title = get_field('address_title','option');
			$address = get_field('address','option');
			
			$social_media_title = get_field('social_media_title','option');						
			?>
            <div class="contact_about">
            	<div class="con_wrap cf">
                    <div class="about_contact_info mob_con">
                        <?php if($contact_title!=""){?><h3><?php echo $contact_title;?></h3><?php }?>
                    </div>    
                                
                	<?php if($about_contact_form!=""){?>
                	<div class="about_form">
                    	<?php echo do_shortcode($about_contact_form);?>
                    </div>
                    <?php }?>
                    
                    <div class="about_contact_info desk_con">
                    	<?php if($contact_title!=""){?><h3><?php echo $contact_title;?></h3><?php }?>
                        
                        <?php if($email_address_title!=""){?>
                        <div class="f_field">
                        	<label for="email"><?php echo $email_address_title;?>:</label>
                            <?php if($email_address!=""){?><p><a href="mailto:<?php echo $email_address;?>"><?php echo $email_address;?></a></p><?php }?>
                        </div>
                        <?php }?>
                        
                        <?php if($telephone_title!=""){?>
                        <div class="f_field">
                        	<label for="addres"><?php echo $telephone_title;?>:</label>
                            <?php if($telephone!=""){?><p><a href="tel:<?php echo $telephone;?>"><?php echo $telephone;?></a></p><?php }?>
                        </div>
                        <?php }?>
                        
                        <?php if($address_title!=""){?>
                        <div class="f_field alladd">
                        	<label for="address"><?php echo $address_title;?>:</label>
                            <?php echo $address;?>
                        </div> 
                        <?php }?>
                        
                        <?php if( have_rows('social_media','option') ): ?>
                        <?php if($social_media_title!=""){?><h4><?php echo $social_media_title;?></h4><?php }?>
                        <ul class="socialmed cf">
                        	<?php while( have_rows('social_media','option') ): the_row(); 
                                $media_select = get_sub_field('media_select');
                                $media_link = get_sub_field('media_link');?>
                        			<li><a href="<?php echo $media_link;?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/<?php echo $media_select;?>.svg" alt="" /></a></li>
                            <?php endwhile;?>
                        </ul>
                        <?php endif;?>
                                                                       
                    </div>
                </div>
            </div>   
            
            <?php $copyright = get_field('copyright','option'); ?>
			<div class="copyright"><?php echo $copyright;?></div>
         
    </div>
    
</div>

<?php get_footer(); ?>