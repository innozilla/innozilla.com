<?php
if ( function_exists('register_sidebars') )
 register_sidebars(2,array(
        'before_widget' => '<li>',
    	'after_widget' => '</li>',
 		'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
register_nav_menus(
array(
'primary-menu' => __( 'Primary' ),
//'footer-menu1' => __( 'Footer Menu 1' ),
//'footer-menu2' => __( 'Footer Menu 2' ),
//'footer-menu3' => __( 'Footer Menu 3' ),
//'footer-menu4' => __( 'Footer Menu 4' ),
//'footer-menu5' => __( 'Footer Menu 5' ),
)
);
}
function widget_mytheme_search() {
?>
        <li>
            <h2>Search</h2>
            <form id="searchform" method="get" action="<?php bloginfo('home'); ?>/"> <input type="text" value="type, hit enter" onfocus="if (this.value == 'type, hit enter') {this.value = '';}" onblur="if (this.value == '') {this.value = 'type, hit enter';}" size="18" maxlength="50" name="s" id="s" /> </form>
        </li>
        <?php
}
if ( function_exists('register_sidebar_widget') )
    register_sidebar_widget(__('Search'), 'widget_mytheme_search');
function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
                <li <?php comment_class(); ?> id="li-comment-
                    <?php comment_ID() ?>">
                    <div id="comment-<?php comment_ID(); ?>">
                        <div class="comment-author vcard">
                            <?php echo get_avatar($comment,$size='32',$default='<path_to_url>' ); ?>

                            <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                        </div>
                        <?php if ($comment->comment_approved == '0') : ?>
                        <em><?php _e('Your comment is awaiting moderation.') ?></em>
                        <br />
                        <?php endif; ?>

                        <div class="comment-meta commentmetadata">
                            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                                <?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>
                            </a>
                            <?php edit_comment_link(__('(Edit)'),'  ','') ?>
                            <?php comment_reply_link(array_merge( $args, array('reply_text' => 'Reply', 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                        </div>

                        <?php comment_text() ?>

                    </div>
                    <?php
        }
define('HEADER_TEXTCOLOR', '');
// define('HEADER_IMAGE', '%s/images/header.jpg'); // %s is theme dir uri - (-- No Image - Shlomo Man --)
define('HEADER_IMAGE_WIDTH', 850);
define('HEADER_IMAGE_HEIGHT', 200);
define( 'NO_HEADER_TEXT', true );

function starter_admin_header_style() {
?>
                            <style type="text/css">
                                #headimg {
                                    background: #fff url(<?php header_image() ?>) no-repeat;
                                }

                                #headimg {
                                    height: <?php echo HEADER_IMAGE_HEIGHT;
                                    ?>px;
                                    width: <?php echo HEADER_IMAGE_WIDTH;
                                    ?>px;
                                }

                                #headimg h1,
                                #headimg #desc {
                                    display: none;
                                }

                            </style>
                            <?php
}
function starter_header_style() {
?>
                                <style type="text/css">
                                    #headerimg {
                                        background: #f0f0f0 no-repeat;
                                    }

                                </style>
                                <?php
}
if ( function_exists('add_custom_image_header') ) {
	add_custom_image_header('starter_header_style', 'starter_admin_header_style');
}
//============================== SCRIPTS AND STYLE
function scriptsandstyles() {
    if ( is_page_template( 'ngrid-template.php' ) ) {
        wp_enqueue_style( 'bootstrap-grid', get_template_directory_uri().'/css/bootstrap.min.css');
    }
 wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/fonts/fontawesome.css');
 wp_enqueue_style( 'fonts', get_template_directory_uri().'/fonts/fonts.css');
 wp_enqueue_style( 'owl', get_template_directory_uri().'/js/owl/owl.css');
 wp_enqueue_style( 'style', get_stylesheet_uri());  
 wp_enqueue_script( 'jquery-3.2.1.min', get_template_directory_uri().'/js/jquery-2.2.4.min.js','','',false);
 wp_enqueue_script( 'all', get_template_directory_uri().'/js/all.js','','',true);
wp_enqueue_script( 'owl', get_template_directory_uri().'/js/owl/owl.js','','',true);


    
  
}
add_action( 'wp_enqueue_scripts', 'scriptsandstyles' );
//============================== Featured Image
add_theme_support( 'post-thumbnails' );
//======================================= Theme Settings Page
if( function_exists('acf_add_options_page') ) {
 $optpage = acf_add_options_page(array(
  'menu_title'  => 'Theme Settings',
  'menu_slug'  => 'theme-settings',
 )); 
}

?>