<?php
/*
Template Name: Template Section Bundle
*/
?>


<?php get_header(); ?>


<?php

// check if the flexible content field has rows of data
if( get_field('sections_template_innozilla') ):

 	// loop through the rows of data
    while ( has_sub_field('sections_template_innozilla') ) :

		 if( get_row_layout() == 'hero_section' ): ?>

        	<section class="hero-wrap" style="background:url('<?php the_sub_field('section_background_hero'); ?>');">
					<div class="anchor" id="home"></div>
					<div class="hero-tint"></div>
						<div class="container">
							<div class="row">
									<div class="col-sm-7 col-xs-12">
										<h1><?php the_sub_field('main_heading_hero'); ?></h1>
										<h2><?php the_sub_field('sub_content'); ?> <span id="typed"></span></h2>
										<div class="hero-cta-container">
											<a href="<?php the_sub_field('call_to_action_link'); ?>"><?php the_sub_field('call_to_action_title_hero'); ?></a>
										</div>
									</div>
								</div>
						</div>
						<div id="typed-strings">
							<?php if( have_rows('type_list_hero') ):
									 while ( have_rows('type_list_hero') ) : the_row(); ?>
									      <p><?php the_sub_field('list_hero');  ?></p>
								<?php endwhile;
								endif; ?>
					    </div>
			</section>


        <?php	
        elseif( get_row_layout() == 'about_section' ): ?>

        	<section class="about-wrap">	
				<div class="anchor" id="about-us"></div>
				<div class="container">
						<h2><?php the_sub_field('main_heading_about');  ?></h2>
						<div class="seperator"></div>
						<?php the_sub_field('sub_content_about');  ?>
				</div>

			</section>
		<?php	

        elseif( get_row_layout() == 'call_to_action_section' ): ?>

			<section id="slogan" class="slogan-wrap" style="background:url('<?php the_sub_field('section_background_cta'); ?>');">
					<div class="anchor" id="slogan"></div>
					<div class="container">
							<div class="row">
									<div class="col-xs-12 col-sm-8">
											<h2><?php the_sub_field('main_content_cta'); ?></h2>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="slogan-cta-container">
											<a href="<?php the_sub_field('call_to_action_link_cta'); ?>"><?php the_sub_field('call_to_action_text'); ?></a>
										</div>
									</div>
							</div>
					</div>

			</section>

		<?php	


        elseif( get_row_layout() == 'services_section' ): ?>

			<section class="services-wrap" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/cartographer.png);">	
					<div class="anchor" id="services"></div>
					<div id="anchor-animate" class="container animated fadeOut">
							<h2><?php the_sub_field('main_heading_services'); ?></h2>
							<div class="seperator"></div>
							<?php the_sub_field('sub_content_services'); ?>

							<div class="row row-centered">

								<?php if( have_rows('services_list') ): ?>

									<?php while ( have_rows('services_list') ) : the_row(); ?>

										<div class="col-sm-4 col-centered">
												<div class="service-single-wrap">
													<?php $icon = get_sub_field('services_icon'); ?>
													<div class="img-control">
														<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
													</div>
													<h3><?php the_sub_field('title_si'); ?></h3>
													<?php the_sub_field('content_si'); ?>
												</div>
										</div>

									<?php endwhile; ?>
								<?php endif; ?>

								

							</div>

					</div>

			</section>



		<?php

        elseif( get_row_layout() == 'partners__clients__list_section' ): ?>


			<section id="specialize" class="specialize-wrap">

					<div class="container animated fadeOut">

						<h3><?php the_sub_field('main_heading_pcl'); ?></h3>
						<div class="seperator"></div>

						<div class="row row-centered">

							<?php if( have_rows('partners_list') ): ?>
									<?php while ( have_rows('partners_list') ) : the_row(); ?>

							<div class="col-xs-6 col-sm-3 col-centered">
							<?php $logo = get_sub_field('logo_pcl'); ?>
								<div class="img-control">
									<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
								</div>

							</div>

								<?php endwhile; ?>
							<?php endif; ?>


						</div>

					</div>

			</section>


		<?php	

        elseif( get_row_layout() == 'pricing_section' ): ?>


			<section class="pricing-wrap" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/cartographer.png);">
				<div class="anchor" id="pricing"></div>
				<div class="container animated fadeOut">

					<h2><?php the_sub_field('main_heading_pricing'); ?></h2>
					<div class="seperator"></div>

					<div class="row row-centered">

						<?php if( have_rows('pricing_list') ): ?>
									<?php while ( have_rows('pricing_list') ) : the_row(); ?>

										<?php if ( get_sub_field('featured_pl') == 0 ) { ?>

											<div class="col-xs-12 col-sm-4 col-centered">

												<div class="main-box-wrap">

													<div class="second-box-wrap">

														<div class="icon-wrap">
															<i class="fa <?php the_sub_field('font_awsome_code_pl'); ?>"></i>
														</div>

														<h4><?php the_sub_field('pricing_title_pl'); ?></h4>
														<h3><?php the_sub_field('price_pl'); ?><span class="currency">usd</span></h3>
														<p class="notes"><?php the_sub_field('notes_pl'); ?></p>

													</div>

													<?php if( have_rows('inclusion_pl') ): ?>

														<ul>

															<?php while ( have_rows('inclusion_pl') ) : the_row(); ?>


																<li><?php the_sub_field('inclusion_list_pl'); ?></li>
			

															<?php endwhile; ?>

														</ul>

													<?php endif; ?>	

													<div class="pricing-cta-container">
														<a href="<?php the_sub_field('call_to_action_link_pl'); ?>"><?php the_sub_field('call_to_action_title_pl'); ?></a>
													</div>

												</div>

											</div>

											<?php } else { ?>

											<div class="col-xs-12 col-sm-4 col-centered featured">

												<div class="main-box-wrap">

													<div class="second-box-wrap">

														<div class="icon-wrap">
															<i class="fa <?php the_sub_field('font_awsome_code_pl'); ?>"></i>
														</div>

														<h4><?php the_sub_field('pricing_title_pl'); ?></h4>
														<h3><?php the_sub_field('price_pl'); ?><span class="currency"><?php the_sub_field('currency_pl'); ?></span></h3>
														<p class="notes"><?php the_sub_field('notes_pl'); ?></p>

													</div>

													<?php if( have_rows('inclusion_pl') ): ?>

														<ul>

															<?php while ( have_rows('inclusion_pl') ) : the_row(); ?>


																<li><?php the_sub_field('inclusion_list_pl'); ?></li>
			

															<?php endwhile; ?>

														</ul>

													<?php endif; ?>	

													<div class="pricing-cta-container">
														<a href="<?php the_sub_field('call_to_action_link_pl'); ?>"><?php the_sub_field('call_to_action_title_pl'); ?></a>
													</div>

												</div>

											</div>

										<?php } ?>

									

							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>

			</section>



		<?php

		elseif( get_row_layout() == 'download' ): ?>





		<?php		
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>




<?php  get_footer(); ?>