<?php
/*
Template Name: Sub Page Template
*/
?>


<?php get_header(); 


// check if the flexible content field has rows of data
if( get_field('sub_page_template') ):

 	// loop through the rows of data
    while ( has_sub_field('sub_page_template') ) :

		 if( get_row_layout() == 'hero_section_sub_page' ): ?>

				<section class="sub-hero-wrap" style="background-color: #000; background:url('<?php the_sub_field('background_image_sub_page_hero'); ?>');">
					<div class="sub-hero-tint"></div>
					<div class="container">

						<h1><?php the_sub_field('main_title_sub_page_hero'); ?></h1>
						<p class="sub"><span><?php the_sub_field('sub_title_sub_page_hero'); ?></span></p>

						<?php the_sub_field('description_sub_page_hero'); ?>

						<div class="cta">
							<a href="<?php the_sub_field('call_to_action_link_sub_page_hero'); ?>"><?php the_sub_field('call_to_action_text_sub_page_hero'); ?></a>
						</div>

					</div>

				</section>

		<?php		
        elseif( get_row_layout() == 'services_description_list' ): ?>

	        	<section class="service-list" style="background-color:<?php the_sub_field('section_background_color_service_list_services'); ?>;">
		
					<div class="container">

							<?php if (!empty(get_sub_field('section_description_service_list_services_description_list'))): ?>
								<p class="section-description"><?php the_sub_field('section_description_service_list_services_description_list'); ?></p>
							<?php endif; ?>
							<div class="row">

							<?php if( have_rows('service_list_services_description_list') ): $counter = 0;  ?>

									<?php 

									while ( have_rows('service_list_services_description_list') ) : the_row();  ?>	
				
											<div class="col-sm-6 list-wrap">
												<div class="row">
														<div class="col-md-3 tr">
																<?php
																$imageico = get_sub_field('image_icon_services_description_list');
																if(empty($imageico)) {
																	the_sub_field('service_icon_services_description_list'); 
																} else { ?>
																 	<img src="<?php echo $imageico['url']; ?>" alt="<?php echo $imageico['alt']; ?>" />
																<?php } ?>

														</div>

														<div class="col-md-9">
																<h3><?php the_sub_field('title_services_description_list'); ?></h3>
																<?php the_sub_field('description_services_description_list'); ?>
														</div>
												</div>
											</div>
									<?php $counter++;
					                  if ($counter % 2 == 0) {
						                 echo '</div><div class="row">';
						              }

								 endwhile; ?>

							<?php endif; ?>

					</div>

					<?php if( !empty(get_sub_field('call_to_action_service_list_services_description_list')) ): ?>
					<div class="cta">
						<a href="<?php the_sub_field('call_to_action_link_service_list_services_description_list'); ?>"><?php the_sub_field('call_to_action_service_list_services_description_list'); ?></a>
					</div>
				<?php endif; ?>

				</section>

        <?php

        elseif( get_row_layout() == 'full_image_repeat' ): ?>

	        	<section class="full-image-repeat"  style="background-color:<?php the_sub_field('section_background_color_full_image_repeat'); ?>;" >
		
					<div  id="anchor-animate" class="container animated">

							<h2><?php the_sub_field('section_title_full_image_repeat'); ?></h2>
							<div class="seperator"></div>

								<?php if( have_rows('full_width_image_feed') ): ?>

									<?php while ( have_rows('full_width_image_feed') ) : the_row();

										$image = get_sub_field('image_feed_full_image_repeat'); ?>	

										<div class="col-xs-12 spacer">
										
											<?php if( !empty($image) ): ?>

												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

											<?php endif; ?>

										
										</div>

									<?php endwhile; ?>

								<?php endif; ?>

					</div>

				</section>

        <?php
        elseif( get_row_layout() == 'table_repeat' ): ?>

        	<section class="pricing" style="background-color:<?php the_sub_field('section_background_color_table_repeat'); ?>;">

				<div class="container">
					<h3 class="intro">Make your website up and running with these web hosting services:</h3>

					<div class="table-responsive">
					   <table class="table table-bordered">
					      <thead>
					         <tr>

					         	<?php if( have_rows('heading_table_repeat') ): ?>
									<?php while ( have_rows('heading_table_repeat') ) : the_row(); ?>
					            
							            <th><?php the_sub_field('title_table_repeat'); ?></th>

							        <?php endwhile; ?>
								<?php endif; ?>

					         </tr>
					      </thead>

					      <tbody>

					      	<?php if( have_rows('secondary_table_parent') ): ?>
								<?php while ( have_rows('secondary_table_parent') ) : the_row(); ?>

							         <tr>
							         	<?php if( have_rows('table_child_secondary_table_parent') ): ?>
											<?php while ( have_rows('table_child_secondary_table_parent') ) : the_row(); ?>
									            <td><?php the_sub_field('content_secondary_table_parent'); ?></td>
									        <?php endwhile; ?>
										<?php endif; ?>
							         </tr>

							    <?php endwhile; ?>
							<?php endif; ?>
					       
					      </tbody>
					   </table>
					</div>
				</div>

			</section>

		<?php
        elseif( get_row_layout() == 'download' ): 

        	$file = get_sub_field('file');

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>




<?php  get_footer(); ?>