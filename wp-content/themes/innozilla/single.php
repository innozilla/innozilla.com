<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Innozilla
 */

get_header(); ?>
	<section class="single-blog-wrap" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/cartographer.png);">
		<div id="primary" class="container">
			<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();
				

				get_template_part( 'template-parts/content', get_post_type() );


			endwhile;
			?>

			</main>
		</div>
	</section>
	<section class="post-yml">
	    <div class="container">
	    	<div class="pyml"> <span>Post you may like</span></div>
	    	<div class="row">
		        <?php
	                $args = array(
	                    'post_type'         => 'post',
	                    'orderby'             => 'rand',
	                    'post__not_in' => array($post->ID),
	                    'posts_per_page'    =>    3
	                );

	                $wp_query = new WP_Query();
	                $wp_query->query( $args );

	              if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	              <div class="col-sm-4">
	              	<?php
				    $thumb_id = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_url($thumb_id,'full', false);
					$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
					$cropped_image = aq_resize($thumb_url, 360, 200, true, true, true ); ?>
					
					<div class="pyml-main-image">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $cropped_image; ?>" alt="<?php echo $alt; ?>"></a>
					</div>
	              	<?php

	                        $categories = get_the_terms( get_the_ID(), 'category' );
	                        $separator = ' / ';
	                        $cats = '';

	                        if ( ! empty( $categories ) ) {
	                           foreach( $categories as $term ) {

	                            $cats .= '<span class="category">' . esc_html( $term->name ) . '</span>' . '<span class="cat-sept">' . $separator . '</span>';

	                            }
	                        }
	                        echo trim( $cats, $separator );

	                    ?>
	              <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	              	
	              </div>

	              <?php
	              endwhile;
	              endif; ?>
	        </div>      
	    </div>
    </section>


<?php
get_footer();
