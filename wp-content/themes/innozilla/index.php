<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Innozilla
 */

get_header();


?>	

<section class="blog-wrap" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/cartographer.png);">
	
	<div class="container">

		<div class="row">
			<div class="col-sm-9 zero-padding">
			<?php $counter = 0;
			while ( have_posts() ) : the_post();
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_url($thumb_id,'full', false);
			$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
			$cropped_image = aq_resize($thumb_url, 350, 210, true, true, true ); ?>
				
						<?php if ( $counter == '0' ) { ?>
						<a href="<?php the_permalink(); ?>">
							<div class="fi-wrap" style="background-image:url(<?php echo $thumb_url; ?>);" >
								
								<div class="float-date">
									<?php
									$month = get_the_time('M');
									$day = get_the_time('j');
									$year = get_the_time('Y');

									?>
									<span class="entry-month"><?php echo $month; ?></span>
									<span class="entry-date"><?php echo $day; ?></span>
									<span class="entry-year"><?php echo $year; ?></span>

								</div>

							</div>
						</a>
						<div class="main-content-wrap">
								<?php

		                            $categories = get_the_terms( get_the_ID(), 'category' );
		                            $separator = ' / ';
		                            $cats = '';

		                            if ( ! empty( $categories ) ) {
		                               foreach( $categories as $term ) {

		                                $cats .= '<i class="fa fa-tag highlight" aria-hidden="true"></i><span class="category">' . esc_html( $term->name ) . '</span>' . '<span class="cat-sept">' . $separator . '</span>';

		                                }
		                            }
		                            echo trim( $cats, $separator );

		                        ?>

								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<p><?php the_excerpt(); ?></p>
							</div>
						<?php } else { ?>

					<div class="row grid2">

						<div class="col-sm-5">
							<a href="<?php the_permalink(); ?>">
							<div class="grid2-image-wrap">
								<div class="float-date">
									<?php
									$month = get_the_time('M');
									$day = get_the_time('j');
									$year = get_the_time('Y');

									?>
									<span class="entry-month"><?php echo $month; ?></span>
									<span class="entry-date"><?php echo $day; ?></span>
									<span class="entry-year"><?php echo $year; ?></span>

								</div>	
									<img src="<?php echo $cropped_image; ?>" alt="<?php echo $alt; ?>">
							</div>
							</a>
						</div>

						<div class="col-sm-7">
								<div class="grid2-wrap">
									<?php

			                            $categories = get_the_terms( get_the_ID(), 'category' );
			                            $separator = ' / ';
			                            $cats = '';

			                            if ( ! empty( $categories ) ) {
			                               foreach( $categories as $term ) {

			                                $cats .= '<i class="fa fa-tag highlight" aria-hidden="true"></i><span class="category">' . esc_html( $term->name ) . '</span>' . '<span class="cat-sept">' . $separator . '</span>';

			                                }
			                            }
			                            echo trim( $cats, $separator );

			                        ?>
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<p><?php the_excerpt(); ?></p>
								</div>
						</div>

					</div>
					<?php } ?>

				
			<?php $counter++; ?>
			<?php endwhile; ?>
			<?php wp_pagenavi(); ?>
			</div>
			<div class="col-sm-3 sidebar-wrap">

				<?php dynamic_sidebar( 'sidebar-1' ); ?>

			</div>

		</div>


	</div>

</section>



<?php
get_footer();
