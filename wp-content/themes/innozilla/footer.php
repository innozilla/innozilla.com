<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Innozilla
 */

?>

	</div><!-- #content -->

	<section class="testimonial-wrap" style="background:url('<?php the_field('section_background_testimonials','option'); ?>');">
			<div class="anchor" id="testimonials"></div>
			<div class="container">

				<div class="slider">


				<?php if( have_rows('testimonials_footer','option') ): ?>

					<?php while ( have_rows('testimonials_footer','option') ) : the_row(); ?>

						<div>
							<?php the_sub_field('client_testimonial','option'); ?>
							<div class="author-wrap">

								<p>
								
								<span class="name"><span class="author-image" style="background:url('<?php the_sub_field('client__company_picture','option') ?>');"></span> <?php the_sub_field('client_name_testimonails','option'); ?> </span> / <span class="position"> <?php the_sub_field('client_postition_testiomonals','option'); ?> </span>
								</p>

							</div>
						</div>


				<?php endwhile;
			endif; ?>


				</div>
			
			</div>

		</section>



	<footer id="colophon" class="site-footer">
		


		<section class="footer-wrap">
			
			<div class="container">

				<div class="row">

					<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
					<div class="col-sm-5 col-sm-push-7">
						<?php dynamic_sidebar( 'footer-4' ); ?>
 
					</div>
					<?php endif; ?>

					<div class="col-sm-7 col-sm-pull-5">

						<div class="row">

							<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
							<div class="col-sm-4">
								<?php dynamic_sidebar( 'footer-1' ); ?>
		 
							</div>
							<?php endif; ?>

							<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
							<div class="col-sm-4">
								<?php dynamic_sidebar( 'footer-2' ); ?>
		 
							</div>
							<?php endif; ?>

							<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
							<div class="col-sm-4">
								<?php dynamic_sidebar( 'footer-3' ); ?>
		 
							</div>
							<?php endif; ?>

						</div>

					</div>


					<div>

				</div>

			</div>


		</section>
		<div class="anchor" id="contact-us"></div>
		<section class="copyright-wrap">
			<div class="anchor" id="copyright"></div>
			<div class="single-content"><?php the_field('copyright','option'); ?></div>

		</section>



	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
