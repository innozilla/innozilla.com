<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Innozilla
 */

get_header(); ?>

	<section class="fof-wrap">
	
		<div class="container">
			
			<h2>404</h2>
			<h3>OOPS! THAT PAGE CAN’T BE FOUND.</h3>
			<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png">

		</div>


	</section>


<?php
get_footer();
