<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Innozilla
 */

?>

<div class="col-sm-4">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
			
				<div class="entry-content">
					<?php 
					$thumb_id = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_url($thumb_id,'full', false);
					$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
					$cropped_image = aq_resize($thumb_url, 360, 200, true, true, true ); ?>
					<div class="single-main-image">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo $cropped_image; ?>" alt="<?php echo $alt; ?>">
						</a>
					</div>
				</div><!-- .entry-content -->

				<header class="entry-header">
					<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
				</header><!-- .entry-header -->
			
	</article>
</div>
<?php wp_pagenavi(); ?>

