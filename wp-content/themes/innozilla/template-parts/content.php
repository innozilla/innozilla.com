<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Innozilla
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php

            $categories = get_the_terms( get_the_ID(), 'category' );
            $separator = ', ';
            $cats = '';

            if ( ! empty( $categories ) ) {
               foreach( $categories as $term ) {

                $cats .= '<span class="category">' . esc_html( $term->name ) . '</span>' . '<span class="cat-sept">' . $separator . '</span>';

                }
            }
            echo trim( $cats, $separator );

        ?>
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php innozilla_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_url($thumb_id,'full', false);
		$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
		$cropped_image = aq_resize($thumb_url, 670, 350, true, true, true ); ?>

		<div class="single-main-image">
			<img src="<?php echo $cropped_image; ?>" alt="<?php echo $alt; ?>">
		</div>
		<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'innozilla' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'innozilla' ),
				'after'  => '</div>',
			) );
		?>

		<div class="sources">
			<div class="heading"><?php the_field('main_title_sources'); ?></div>
				<?php if( have_rows('sources') ): ?>
					<?php while( have_rows('sources') ): the_row(); ?>
						<div class="content"><a href="<?php the_sub_field('source_link'); ?>" target="_blank" rel="nofollow"><?php the_sub_field('source_title'); ?></a></div>
					<?php endwhile; ?>
				<?php endif; ?>
		</div>

		<div class="social-wrap">
			<h3>If you like our blog posts, <br>share them to your friends.</h3>
			<div class="social-icon-wrap">
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook."><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
					<a target="_blank" href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Share on LinkedIn"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					<a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
			</div>
		</div>

		<div id="disqus_thread"></div>
		<script>

		/**
		*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
		*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
		/*
		var disqus_config = function () {
		this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
		this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
		};
		*/
		(function() { // DON'T EDIT BELOW THIS LINE
		var d = document, s = d.createElement('script');
		s.src = 'https://innozilla.disqus.com/embed.js';
		s.setAttribute('data-timestamp', +new Date());
		(d.head || d.body).appendChild(s);
		})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
