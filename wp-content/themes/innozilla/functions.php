<?php
/**
 * Innozilla functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Innozilla
 */

if ( ! function_exists( 'innozilla_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function innozilla_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Innozilla, use a find and replace
		 * to change 'innozilla' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'innozilla', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'innozilla' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'innozilla_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'innozilla_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function innozilla_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'innozilla_content_width', 640 );
}
add_action( 'after_setup_theme', 'innozilla_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function innozilla_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Col 1', 'innozilla' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'innozilla' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Col 2', 'innozilla' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'innozilla' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Col 3', 'innozilla' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'innozilla' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Contact Form', 'innozilla' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'innozilla' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Post Sidebar', 'innozilla' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'innozilla' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'innozilla_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function innozilla_scripts() {

	wp_enqueue_style( 'innozilla-style', get_stylesheet_uri() );

	wp_enqueue_script( 'innozilla-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_style('animate-css', get_template_directory_uri() . '/css/animate.css');

	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css');

	wp_enqueue_style('slick-css', get_template_directory_uri() . '/css/slick.css');

	wp_enqueue_style('slick-theme', get_template_directory_uri() . '/css/slick-theme.css');

	if ( is_front_page() ) :

		wp_enqueue_script( 'custom-js-home', get_stylesheet_directory_uri() . '/js/vendor-home.js', array( 'jquery' ) );

	endif;

	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/vendor.js', array( 'jquery' ) );

	wp_enqueue_script( 'innozilla-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'waypoint-js', get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js', array( 'jquery' ) );

	if ( !(is_home()) ) :
		wp_enqueue_script( 'typed-js', get_stylesheet_directory_uri() . '/js/typed.min.js', array( 'jquery' ) );
	endif;

	wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/js/slick.min.js', array( 'jquery' ) );

	wp_enqueue_script( 'matchheight', get_stylesheet_directory_uri() . '/js/jquery.matchHeight-min.js', array( 'jquery' ) );

    wp_enqueue_script( 'matchheight-list', get_stylesheet_directory_uri() . '/js/matchheight-init.js', array( 'jquery' ) );

    wp_enqueue_style( 'font-awesome', get_template_directory_uri() .'/css/font-awesome.min.css', array(), '4.7.0' );

	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery') );

	wp_enqueue_script('js-easing', get_template_directory_uri().'/js/jquery.easing.min.js', array('jquery') );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'innozilla_scripts' );

// GOOGLE FONTS

function wpb_add_google_fonts() {

	wp_enqueue_style( 'wpb-google-fonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700', false ); 
	wp_enqueue_style( 'wpb-google-fonts-PTserif', 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i', false ); 

}

add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );


//ADDING META KEYWORDS TO HEAD

require_once('template-parts/aq_resizer.php');

function myCallbacktoAddMeta(){

  $values = get_field('keywords_settings');
  echo '<meta name="keywords" content="' . $values .'" />';

}
add_action('wp_head', myCallbackToAddMeta,'1');

//ADDING CUSTOM LOGO TO LOGIN PAGE

function my_login_logo() { ?>
    <style type="text/css">
	    body.login.login {
		    background: #252525;
		}
	    #login h1  {
	    	margin: 0px 30px;
	    }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
            width: 100%;
    		background-size: contain;
    		margin: 0px
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


//ADD THEME OPTIONS

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

//ADD CODE TO HEAD

function child_theme_head_script() {
	the_field('code_to_head','option');
}
add_action( 'wp_head', 'child_theme_head_script' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

