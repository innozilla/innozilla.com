jQuery(document).ready(function($){


	//Mobile Menu Animation

	  $(".navbar-toggle").on("click", function () {
		    $(this).toggleClass("active");
	  });

	  $(document).mouseup(function(e){  
	  var menu = $('.menu-primary-menu-container');
	  var menuz = $('.navbar-toggle');
	  if ( $( "#site-navigation" ).hasClass( "toggled" ) ) {
		  if (!menu.is(e.target) && !menuz.is(e.target) // The target of the click isn't the container.
		    && menu.has(e.target).length === 0 && menuz.has(e.target).length === 0) // Nor a child element of the container
		    {
		      $("#site-navigation").removeClass("toggled");
		      
		      		$(".navbar-toggle").toggleClass("active");
		  	 }
	  }
  	});

	// sticky menu
	if (screen && screen.width > 1024) {
	  var  mn = $("#masthead");
      mns = "main-nav-scrolled";
      mnsz = "exit";
      hdr = $('header').height();

	  $(window).scroll(function() {
	    if( $(this).scrollTop() > hdr ) {
	      mn.addClass(mns);
	      mn.removeClass(mnsz);
	    } else {
	      mn.removeClass(mns);
	      mn.addClass(mnsz);
	    }
	  });
	 }


	 // Smooth Scrolling

     $('a[href*="#"]:not([href="#"])').click(function() {
         if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
             var target = $(this.hash);
             target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
             if (target.length) {
                 $('html, body').animate({
                     scrollTop: target.offset().top
                 }, 800, 'easeInBack');
                 return false;
             }
         }
     });		

    // Add class to active anchor - One pager
	if ( $( "body" ).hasClass( "home" ) ) {
		
		function updateMenu() {

	    var lastId,
	        mainMenu = $("#primary-menu"),
	        mainMenuInnerHeight = mainMenu.outerHeight(),
	        mainMenuItems = $("#primary-menu").find("a[href*=#]"),        

	        scrollItems = $("#primary-menu li a").map(function() {
	            itemsSplit = $(this).attr("href").split("#");
	            itemHref = $("#" + itemsSplit[1]);
	            if (itemHref.length) {
	            return itemHref;
	        	}
	        });     

	    mainMenuItems.bind("click",scrollDown);

	    function scrollDown(e){
	        e.preventDefault(); 

	        var href = $(this).attr("href").split("#"),
	            hrefSplit = href[1],

	            offsetTop = $("#" + hrefSplit).offset().top;        

	        $("html, body").stop().animate({ 
	            scrollTop: offsetTop
	        }, 1000, 'easeInOutQuint');
	    }   

	    $(window).scroll(function(){

	        var fromTop = $(this).scrollTop()+mainMenuInnerHeight;

	        var cur = scrollItems.map(function(){               
	            if ($(this).offset().top < fromTop) 
	                return this;
	        });

	        cur = cur[cur.length-1];

	        var id = cur && cur.length ? cur[0].id : "";

	        if (lastId !== id) {
	            lastId = id;

	            mainMenuItems
	                .parent().removeClass("active")
	                .end().filter("[href$='#" + id + "']").parent().addClass("active");
	        }                   
		    });
		}

		updateMenu();   
		$(window).resize(function(){
		    updateMenu();
		});

	} 
	   //
		   // hide our element on page load 
		  $('#anchor-animate').waypoint(function(direction) {
		    if (direction === 'down') {
		      // reveal our content
		      $('#anchor-animate').addClass('animated fadeIn');
		      $('#anchor-animate').removeClass('fadeOut');
		    } else if (direction === 'up') {
		      // hide our content
		      $('#anchor-animate').addClass('animated fadeOut');
		      $('#anchor-animate').removeClass('fadeIn');
		    }
		 
		  }, { offset: '80%' });

		  //
		  $('.specialize-wrap .container').waypoint(function(direction) {
		    if (direction === 'down') {
		      // reveal our content
		      $('.specialize-wrap .container').addClass('animated fadeIn');
		      $('.specialize-wrap .container').removeClass('fadeOut');
		    } else if (direction === 'up') {
		      // hide our content
		      $('.specialize-wrap .container').addClass('animated fadeOut');
		      $('.specialize-wrap .container').removeClass('fadeIn');
		    }
		 
		  }, { offset: '70%' });

		  //
		  $('.pricing-wrap .container').waypoint(function(direction) {
		    if (direction === 'down') {
		      // reveal our content
		      $('.pricing-wrap .container').addClass('animated fadeIn');
		      $('.pricing-wrap .container').removeClass('fadeOut');
		    } else if (direction === 'up') {
		      // hide our content
		      $('.pricing-wrap .container').addClass('animated fadeOut');
		      $('.pricing-wrap .container').removeClass('fadeIn');
		    }
		 
		  }, { offset: '60%' });


	 // slick slider

	 $('.slider').slick({
	 	infinite: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay: true,
  		autoplaySpeed: 8000,
        prevArrow: "<i class='fa fa-angle-left slick-prev' aria-hidden='true'></i>",
        nextArrow: "<i class='fa fa-angle-right slick-next' aria-hidden='true'></i>",
	});

	 


});



    