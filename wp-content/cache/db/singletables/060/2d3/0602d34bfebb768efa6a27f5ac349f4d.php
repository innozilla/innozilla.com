{�]<?php exit; ?>a:6:{s:10:"last_error";s:0:"";s:10:"last_query";s:69:"SELECT wp_posts.* FROM wp_posts WHERE ID IN (436,417,375,286,276,251)";s:11:"last_result";a:6:{i:0;O:8:"stdClass":23:{s:2:"ID";s:3:"251";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-07 09:20:46";s:13:"post_date_gmt";s:19:"2017-11-07 09:20:46";s:12:"post_content";s:9256:"How to speed up WordPress surely is a hot topic these days. Fortunately, there are numerous techniques that you can employ to get the job done.
<h2>1. Choose a better web hosting provider</h2>
The major factor that influences the speed of a website is the hosting of your WordPress website. It might seem like a good idea to host your new website on a shared hosting provider that offers “unlimited” bandwidth, space, emails, domains and more. However, the point that we usually miss out on regarding this offer is that shared hosting environments fail to deliver good loading times on peak traffic hours, and most fail to provide 99 percent uptime in any given month.

Shared hosting tends to deliver a poorer performance because you are sharing the same server space with countless other websites, and there is no telling how much resources others are using. Plus, you don’t know exactly how well the servers are optimized.

Thankfully, the web-hosting industry has advanced with technology, and the prices of cloud hosting providers have decreased with the passage of time. In the present times, you can buy dedicated cloud servers from <a class="external" href="https://www.siteground.com/go/codeinwp-special" target="_blank" rel="nofollow noopener">SiteGround</a>, DigitalOcean, Amazon Web Services, and even Google Compute Engine at a nominal price. However, setting those servers up can be a daunting task as you are required to set servers up from scratch. There are web hosting providers like Cloudways <em>(where I work)</em> who make the task of setting up optimized cloud servers as easy as click and launch. You can read more about the process here: <a class="external" href="http://www.cloudways.com/blog/wordpress-on-google-cloud/" rel="nofollow">how to set up WordPress through Cloudways</a>.
<h2>2. Use a lightweight WordPress theme / framework</h2>
WordPress themes with a lot of dynamic elements, sliders, widgets, social icons and many more shiny elements are immensely appealing to the eye. But remember this: if they have too many elements and higher page sizes, then they will definitely cause your web server to take a thumping.

The best option here is to use lightweight themes, like WordPress’ default themes.The new Twenty Fifteen theme is always a good way to start off a blog. For a feature-rich website, you can also opt for a theme that uses a good framework like Bootstrap or Foundation. For instance, all themes at <a class="external" href="https://themeisle.com/" rel="nofollow">ThemeIsle</a> are built on top of Bootstrap, which provides a great way to speed up WordPress.
<h2>3. Reduce image sizes</h2>
Images are the major contributors to size increment of a given webpage. The trick is to reduce the size of the images without compromising on the quality.

If you manually optimize the images using Chrome PageSpeed Insights extension or Photoshop or any other tools, the process will take a long time. Fortunately, there are plugins available for just about everything you can think of, including image optimization. The ones worth mentioning are:
<ul>
 	<li><a class="external" href="https://wordpress.org/plugins/wp-smushit/" rel="nofollow">WP Smush</a></li>
 	<li><a class="external" href="https://wordpress.org/plugins/ewww-image-optimizer/" rel="nofollow">EWWW Image Optimizer</a></li>
</ul>
Using any of the above mentioned plugins on your WordPress site will drastically reduce image sizes, thus improving the speed of your website.
<h2>4. Minify JS and CSS files</h2>
If you run your website through Google PageSpeed Insights tool, you will probably be notified about minimizing the size of your CSS and JS files. What this means is that by reducing the number of CSS and JS calls and the size of those files, you can improve the site-loading speed.

Also, if you know your way around WordPress themes, you can study the guides <a class="external" href="https://developers.google.com/speed/docs/insights/MinifyResources" rel="nofollow">provided by Google</a> and do some manual fixing. If not, then there are plugins that will help you achieve this goal; the most popular being the <a class="external" href="https://wordpress.org/plugins/autoptimize/" rel="nofollow">Autoptimize</a> that can help in optimizing CSS, JS and even HTML of your WordPress website.
<h2>5. Use advanced caching mechanisms with a caching plugin</h2>
WordPress caching plugins (e.g. W3 Total Cache) have been there for a long time, making the complex tasks of adding caching rules to your website elements easier. Combining such plug-ins with advanced caching mechanisms like Varnish could help you better the loading-speed of your website and ultimately speed up WordPress considerably.
<h2>6. Use a CDN</h2>
The people who visit your website belong to various locations in the world, and needless to say, the site-loading speed will differ if the visitors are located far away from where your site is hosted. There are many CDN (Content Delivery Networks) that help in keeping the site-loading speed to a minimum for visitors from various countries. A CDN keeps a copy of your website in various datacenters located in different places. The primary function of a CDN is to serve the webpage to a visitor from the nearest possible location. Cloudflare and MaxCDN are among the most popular CDN services.
<h2>7. Enable GZIP compression</h2>
Compressing files on your local computer can save a lot of disk space. Similarly, for the web, we can use GZIP compression. This maneuver will dramatically reduce the bandwidth usage and the time it takes to gain access to your website. GZIP compresses various files so that whenever a visitor tries to access your website; their browser will first have to unzip the website. This process brings down the bandwidth usage to a considerable extent.

You can use either a plugin like the <a class="external" href="https://wordpress.org/plugins/gzip-ninja-speed-compression/" rel="nofollow">GZip Ninja Speed Optimization</a> or add the following codes in your .htaccess file.
<pre><code>AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript</code></pre>
<h2>8. Cleanup WordPress database</h2>
Deleting unwanted data from your database will keep its size to a minimum and also helps in reducing the size of your backups. It is also necessary to delete spam comments, fake users, old drafts of your content and maybe even unwanted plugins as well as themes. All of this will reduce the size of your databases and web files, and thus speed up WordPress – your WordPress.
<h2>9. Deactivate or uninstall plugins</h2>
Keeping unwanted plugins on your WordPress websites will add a tremendous amount of junk to your web files. Moreover, it will also increase the size of your backup and put an overwhelming amount of load on your server resources while backup files are being generated. It is better to get rid of the plugins that you don’t use, and also look for alternate methods to use third-party services for automating or scheduling tasks (like sharing of your latest posts to social media).

IFTTT or Zapier are two web services that help in automating such tasks and reduce the burden on your website and server resources.
<h2>10. Keep external scripts to a minimum</h2>
The usage of external scripts on your web pages adds a big chunk of data to your total loading time. Thus, it is best to use a low number of scripts, including only the essentials such as tracking tools (like Google Analytics) or commenting systems (like Disqus).
<h2>11. Disable pingbacks and trackbacks</h2>
Pingbacks and trackbacks are two core WordPress components that alert you whenever your blog or page receives a link. It might sound useful, but you also have things such as Google Webmaster Tools and other services to check the links of your website.

Keeping pingbacks and trackbacks on can also put an undesirable amount of strain on your server resources. This is so because whenever anyone tries to link up to your site, it generates requests from WordPress back and forth. This functionality is also widely abused when targeting a website with DDoS attacks.

You can turn it all off in <em>WP-Admin -&gt; Settings -&gt; Discussion</em>. Just deselect “<em>Allow link notifications from other blogs (pingbacks and trackbacks).</em>” This will help you speed up WordPress some more.
<h2>Conclusion</h2>
The biggest advantage of lowering your website’s loading time is that it will help tremendously in improving the experience of your visitors. The case remains the same whether they are using mobile devices or PCs. Furthermore, it will also improve your rankings in the SERPs. After all, reduced bandwidth usage of your hosting and faster site-loading speed on the client side will only benefit you both in the short as well as the long run.";s:10:"post_title";s:35:"11 Ways to Boost Wordpress Websites";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:32:"11-ways-boost-wordpress-websites";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-11-07 14:16:05";s:17:"post_modified_gmt";s:19:"2017-11-07 14:16:05";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://innozilla.com/?p=251";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:1;O:8:"stdClass":23:{s:2:"ID";s:3:"276";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-14 08:49:25";s:13:"post_date_gmt";s:19:"2017-11-14 08:49:25";s:12:"post_content";s:6555:"What browser caching does is "remember" the resources that the browser has already loaded. When a visitor goes to another page on your website your logo, CSS files, etc. do not need to be loaded again, because the browser has them "remembered" (saved). This is the reason that the <a href="https://varvy.com/pagespeed/first-view.html">first view</a> of a web page takes longer than repeat visits.

When you leverage browser caching, your webpage files will get stored in the <a href="https://varvy.com/performance/application-cache.html">browser cache</a>. Your pages will load much faster for repeat visitors and so will other pages that share those same resources.

If you have tested your webpage for speed and found out that you need to leverage browser caching, here is how you do it
<h2>How to leverage browser caching</h2>
<ol>
 	<li>Change the request headers of your resources to use caching.</li>
 	<li>Optimize your caching strategy.</li>
</ol>
<h2>Change the request headers of your resources to use caching</h2>
For most people, the way to enable caching is to add some code to a file called .htaccess on your web host/server.

This means going to the file manager (or wherever you go to add or upload files) on your webhost.

The .htaccess file controls many important things for your site. If you are not familiar with the .htaccess file, please read my <a href="https://varvy.com/pagespeed/htaccess.html">working with .htaccess</a> article to get some know how before changing it.
<h3>Browser caching for .htaccess</h3>
The code below tells browsers what to cache and how long to "remember" it. It should be added to the top of your .htaccess file.
<pre>
<code>
## EXPIRES CACHING ##
&lt;IfModule mod_expires.c&gt;
ExpiresActive On
ExpiresByType image/jpg "access 1 year"
ExpiresByType image/jpeg "access 1 year"
ExpiresByType image/gif "access 1 year"
ExpiresByType image/png "access 1 year"
ExpiresByType text/css "access 1 month"
ExpiresByType text/html "access 1 month"
ExpiresByType application/pdf "access 1 month"
ExpiresByType text/x-javascript "access 1 month"
ExpiresByType application/x-shockwave-flash "access 1 month"
ExpiresByType image/x-icon "access 1 year"
ExpiresDefault "access 1 month"
&lt;/IfModule&gt;
## EXPIRES CACHING ##
</code>
</pre>
Save the .htaccess file and then refresh your webpage.
<h3>How to set different caching times for different file types</h3>
You can see in the above code that there are time periods like "1 year" or "1 month". These are associated with file types, as an example the above code states that a .jpg file (image) should be cached for a year.

If you want to change that and say you want your jpg images cached for a month you would simply replace "1 year" with "1 month". The values above are pretty optimized for most webpages and blogs.
<h3>Alternate caching method for .htaccess</h3>
The above method is called "Expires" and it works for most people using .htaccess so it takes care of caching for most people who are just getting started.

After you are more comfortable with caching, you may want to try Cache-Control, another method of caching which gives us more options.

It is also possible the expires method did not work for your server, in that case you may want to try to use Cache-Control.
<h2>Cache-Control</h2>
Note: I have made a more complete guide to <a href="https://varvy.com/pagespeed/cache-control.html">Cache-Control here</a>.

Cache-Control allows us to have a bit more control of our browser caching and many people find it easier to use once setup.

Example use in .htaccess file:
<pre>
<code>
# 1 Month for most static assets
&lt;filesMatch ".(css|jpg|jpeg|png|gif|js|ico)$"&gt;
Header set Cache-Control "max-age=2592000, public"
&lt;/filesMatch&gt;
</code>
</pre>

The above code is setting a cache control header depending on file type.
<h3>How cache-control works</h3>
Let's take the above code line by line.
<pre>
<code>
# 1 Month for most static assets
</code>
</pre>


The above line is just a note. It does not do anything except notate what we are doing. The .htaccess file ignores lines that start with the character #. This note is recommended as you may have several different sets of these as your caching solution grows.
<pre>
<code>
&lt;filesMatch ".(css|jpg|jpeg|png|gif|js|ico)$"&gt;
</code>
</pre>
The above line is saying that "if the file is one of these types, then we will do something to it...

The important part of this line is to notice that there are different types of files listed (css, js, jpeg, png, etc.) and that the caching instructions that follow will apply to those file types. As an example, if you did not want your jpg files to be cached for this amount of time you could delete "jpg" from this line or if you wanted to add html to this you could just add "html" to this line.
<pre>
<code>
Header set Cache-Control "max-age=2592000, public"
</code>
</pre>
The above line is where the actual headers are inserted and the values given.
<ul>
 	<li>The "Header set Cache-Control" part is setting a header.</li>
 	<li>The "max-age=2592000" part is stating how long it should be cached (using seconds). In this case we are caching for one month which is "2592000" seconds.</li>
 	<li>The "public" part is stating that this is public (which is good if you want it to be cached).</li>
</ul>
<pre>
<code>
&lt;/filesMatch&gt;
</code>
</pre>
The above line is closing the statement and ends the block of code.
<h2>Common caching issue</h2>
If you list your html and images to be cached for one year or some other long time period, remember that this can mean if you make a change to your pages they may not be seen by all users. This is because the users will look to cached files rather than the live ones. If you have file that you tweak occasionally (example - a CSS file) you can overcome the cache issue by using URL fingerprinting.
<h2>URL fingerprinting</h2>
Getting a fresh (not cached) file resource is possible by having a unique name. An example would be if our css file was named "main.css" we could name it "main_1.css" instead. The next time we change it we can call it "main_2.css". This is useful for files that change occasionally.
<h2>Caching methods</h2>
It is important to specify one of Expires or Cache-Control max-age, and one of Last-Modified or ETag, for all cacheable resources. It is redundant to specify both Expires and Cache-Control: max-age, or to specify both Last-Modified and ETag.";s:10:"post_title";s:24:"Leverage browser caching";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:24:"leverage-browser-caching";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-11-15 09:04:17";s:17:"post_modified_gmt";s:19:"2017-11-15 09:04:17";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://innozilla.com/?p=276";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:2;O:8:"stdClass":23:{s:2:"ID";s:3:"286";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-15 09:09:58";s:13:"post_date_gmt";s:19:"2017-11-15 09:09:58";s:12:"post_content";s:4172:"Enabling gzip compression is a standard practice. If you are not using it for some reason, your webpages are likely slower than your competitors.
<h2>How to enable Gzip compression</h2>
<ul>
 	<li>Compression is enabled via webserver configuration</li>
 	<li>Different web servers have different instructions (explained below)</li>
</ul>
Here are the most common ways to enable compression including .htaccess, Apache, Nginx, and Litespeed webservers.
<h2>Enable compression via .htaccess</h2>
For most people reading this, compression is enabled by adding some code to a file called .htaccess on their web host/server. This means going to the file manager (or wherever you go to add or upload files) on your webhost.

The .htaccess file controls many important things for your site. If you are not familiar with the .htaccess file, please read my <a href="https://varvy.com/pagespeed/htaccess.html">working with .htaccess</a> article to get some know how before changing it.

The code below should be added to your .htaccess file...

<pre>
<code>
&lt;ifModule mod_gzip.c&gt;
mod_gzip_on Yes
mod_gzip_dechunk Yes
mod_gzip_item_include file .(html?|txt|css|js|php|pl)$
mod_gzip_item_include handler ^cgi-script$
mod_gzip_item_include mime ^text/.*
mod_gzip_item_include mime ^application/x-javascript.*
mod_gzip_item_exclude mime ^image/.*
mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
&lt;/ifModule&gt;
</code>
</pre>

Save the .htaccess file and then refresh your webpage.

Check to see if your compression is working using the <a href="https://varvy.com/tools/gzip/">Gzip compression tool</a>.
<h2>Enable compression on Apache webservers</h2>
The instructions and code above will work on Apache. If they are not working there is another way that may work for you. If the above code did not seem to work, remove it from your .htaccess file and try this one instead...
<pre>
<code>
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
</code>
</pre>

<h2>Enable compression on NGINX webservers</h2>
To enable compression in NGINX you will need to add the following code to your config file

<pre>
<code>
gzip on;
gzip_comp_level 2;
gzip_http_version 1.0;
gzip_proxied any;
gzip_min_length 1100;
gzip_buffers 16 8k;
gzip_types text/plain text/html text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;

# Disable for IE &lt; 6 because there are some known problems
gzip_disable "MSIE [1-6].(?!.*SV1)";

# Add a vary header for downstream proxies to avoid sending cached gzipped files to IE6
gzip_vary on;
</code>
</pre>

<h2>Enable compression on Litespeed webservers</h2>
The ideal way to enable compression in Litespeed is to do it through the configuration under "tuning". Just go down to "enable compression" and check to see if it is on, if not click "edit" then choose to turn it on. While you are there, look over the several Gzip options that are nearby.
<h2>How effective is gzip compression?</h2>
Compression of your HTML and CSS files with gzip typically saves around fifty to seventy percent of the file size. This means that it takes less time to load your pages, and less bandwidth is used over all.
<h2>How compressed files work on the web</h2>
When a request is made by a browser for a page from your site your webserver returns the smaller compressed file if the browser indicates that it understands the compression. All modern browsers understand and accept compressed files.
<h2>Testing compression</h2>
To see if gzip compression is working use our <a href="https://varvy.com/tools/gzip/">gzip compression tool</a>.

Also consider using the <a href="https://varvy.com/pagespeed/">page speed tool</a> which will test compression and many other factors.";s:10:"post_title";s:23:"Enable gzip compression";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:23:"enable-gzip-compression";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-11-15 09:09:58";s:17:"post_modified_gmt";s:19:"2017-11-15 09:09:58";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://innozilla.com/?p=286";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:3;O:8:"stdClass":23:{s:2:"ID";s:3:"375";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-08 02:31:20";s:13:"post_date_gmt";s:19:"2018-05-08 02:31:20";s:12:"post_content";s:11761:"Your Internet experience is only as fast as the websites you visit. Typically, the best websites are also the ones with the quickest load times. A slow website makes for a terrible user experience, and let’s not forget the impact that it is likely to have on your search engine rankings. <a href="https://googlewebmastercentral.blogspot.in/2010/04/using-site-speed-in-web-search-ranking.html" rel="nofollow external" data-wpel-link="external">Google has confirmed the same in the past</a>.

If you run a WordPress site, a caching plugin can dramatically effect better site load times. Typically, when you access any website, you request information from their servers. WordPress runs of a database, and every  time someone loads your site on their browser, they retrieve files in the form of CSS, images, and JavaScript.

WordPress is dynamic. While this helps keep your website up to date and live, it also slows down your site. So, to tackle this little problem, developers created caching plugins. They help produce a static version of your website and this makes your website much much faster.

I’ll take you through a few plugins that can help speed up your WordPress site. If you aren’t currently using a caching plugin, you’ll certainly find it an interesting and rather important read. And if you are using a caching plugin then you’ll have a few more options to check out and find out which plugin works best for you.

If all these caching plugins fail you and it doesn’t make your site any quicker, then you’ll probably have to upgrade your host. But before you do so, try a few caching plugins and see how they work for your website.
<h2>W3 Total Cache</h2>
<img class="alignnone size-full wp-image-377" src="https://innozilla.com/wp-content/uploads/2018/05/W3-Total-Cache-Header1.png" alt="" width="800" height="260" />

W3 Total Cache is popular as one of the most powerful caching plugins with a plethora of options at the disposal of the user. Its users include Yoast (author behind the most popular WordPress SEO plugin, which we use on Colorlib as well), Matt Cutts (works on search quality and web spam for Google), Mashable, Smashing Magazine, and many other equally influential websites.

<strong>Update (1):</strong> Yoast no longer uses plugin based caching and have switched to server side caching. Likely a similar caching we use for Colorlib which is combination between HHVM, Redis and fastcgi_cache.

<strong>Update (2):</strong> Matt Cutts has switched from W3 Total Cache to WP Super Cache. It’s similar to W3 but is much easier to setup and according to some benchmarks can deliver even better website performance under certain conditions.

<strong>Update (3): </strong>We now use a completely custom built server side caching. It uses Nginx, HHVM and Redis as base but we have made some tweaks specially for Colorlib. This includes custom comment and bbPress forum thread handling to make sure that they are posted in real time. If you like how well our website perform, make sure to read our hosting guide to find out how you can achieve similar results.

It can cache pages, the WordPress database, and objects. It can enable caching at the browser end. Use W3 Total Cache for the minification of CSS and JavaScript. It is also compatible with dedicated servers, virtual private servers, and content delivery networks, which is probably why you can scale this plugin for use with very popular high-traffic websites.

A WordPress newbie may find it daunting to negotiate through the plugin’s many options. That being said, W3 Total Cache offers tips on how to best use the plugin above the plugin’s settings page on your WP dashboard.

<img class="alignnone size-full wp-image-376" src="https://innozilla.com/wp-content/uploads/2018/05/W3-Total-Cache-Guide.png" alt="" width="800" height="748" />

The tips actually seem more daunting than the advanced options themselves, how well you use this plugin depends on your ability to attend to each instruction provided. At the bottom, you’ll notice a “Toggle all caching types”, this is sufficient but doesn’t maximise the impact of the plugin.

Even an experienced user would find the instructions helpful because installing the plugin is only part of the process, it needs to be configured appropriately for full impact. Else you can simply choose to enable the default settings, it should be sufficient for most websites.

The plugin isn’t complicated, it is merely vast with a number of options. If you can get past the clutter of options and follow suggestions as presented by the plugin below your WordPress dashboard and may be watch a <a href="https://www.youtube.com/watch?v=Mz7TStqciwI" rel="nofollow external" data-wpel-link="external">tutorial</a> or <a href="https://www.youtube.com/watch?v=LVteYk-rG-A" rel="nofollow external" data-wpel-link="external">two</a> then it can speed up your website ten times over, which is an impressive feat.

At $99 per year for the paid version, you can enable fragment caching module for better performance with themes/plugins that use WordPress transient API and the paid version also provides CDN mirroring.
<h2>WP Super Cache</h2>
<img class="alignnone size-full wp-image-378" src="https://innozilla.com/wp-content/uploads/2018/05/WpSuperCacheHeader.png" alt="" width="800" height="258" />

When I first used this plugin, it was far easier to figure stuff out than the previous plugin. The plugin settings screen greets you with an easy to use version of the plugin. There are multiple tabs, the first one titled “Easy” is displayed first. And it is easier when you aren’t bombarded with as many options as with W3 Total Cache.

<img class="alignnone size-full wp-image-379" src="https://innozilla.com/wp-content/uploads/2018/05/WpSuperCache1.png" alt="" width="800" height="412" />

WP Super Cache creates a static HTML file which is served to users who aren’t logged in, users who haven’t left a comment on your blog and users who haven’t viewed a password protected post on your site. That pretty much means almost every visitor to your website.

This plugin caches files in three ways:
<ol>
 	<li>Supercached Static files – PHP is completely bypassed and it served as such to unknown visitors.</li>
 	<li>Supercached Static files ( served by PHP ) – Server more likely to struggle with large increase or bursts of traffic.</li>
 	<li>Legacy Caching – Slowest caching method used for known users.</li>
</ol>
The difference between supercached served by PHP and not served by PHP becomes more apparent only when there is an increase in traffic, so much so that the host’s server struggles to keep up, else the differences are imperceptible.

You can selectively choose which sections of your website get cached. The plugin also handles sudden spikes in traffic using lockdown and directly cached files.

<img class="alignnone size-full wp-image-380" src="https://innozilla.com/wp-content/uploads/2018/05/WpSuperCache02.png" alt="" width="800" height="1248" />

WP Super Cache helps get rid of your cache files as well by deleting them at specified intervals of time. Preloading the files will create supercached static files for your most recently published posts or for every page and post on your site. With preloading, dealing with cached files after they become redundant is all the more important.
<h2>Which One Is The Best One?</h2>
I’ve looked at the tests conducted by different people with differing recommendations for caching plugin.

Those tests do provide a great deal of information, however they do not include WP Rocket which I feel would have certainly been just as good as the competition.  This test is very comprehensive, we compared several caching solutions for WordPress. WP Rocket came out as the winner, with special mentions for WP Super Cache and W3 Total Cache.

The fact is it is very difficult to figure out which among all the plugins is the best. In my opinion, WP Rocket certainly seems to have the edge. But rest of the pack aren’t too far behind at all.

If you’re selecting a caching plugin, then you should consider your requirements. Whether you’d need CDN support? Questions like this become pertinent to selection of the best plugin for your website. Because the differences in performance is largely imperceptible to average user.
<h2>Test Your Site With A Cache Plugin Fully Configured.</h2>
You can use one of the following,
<ul>
 	<li><a href="https://gtmetrix.com/" rel="nofollow external" data-wpel-link="external">GTmetrix</a></li>
 	<li><a href="http://www.webpagetest.org/" rel="nofollow external" data-wpel-link="external">WebPageTest</a></li>
 	<li><a href="https://developers.google.com/speed/pagespeed/" rel="nofollow external" data-wpel-link="external">PageSpeed Tools</a></li>
</ul>
To keep track of your website regularly, you can access site performance under labs via your Google Analytics account, if you’ve added your website to GA. And most hosting services also provide access to basic page load speed information.

But if you are insistent on running a cache plugin and benchmarking your website thoroughly, it would come in handy if you understood the benchmarking methods used on our post.  You’ll see that performing a thorough check is rather tiresome. It is only worth it, if you’re website receives sufficient traffic. Else one of the three aforementioned tools should be sufficient.

Based on your level of comfort with caching plugins you can select any one of the 6 plugins presented in this post. Try each one of them (even the paid ones have refund periods) and arrive at what works for you.

Be sure to use the same theme, plugins and same host for your tests. And also test a number of URLs (apart from just your website’s homepage) for some variability and to ensure that the test is fair.
<h2>Conclusion</h2>
Choose the plugin that you are comfortable with, spending an hour each day to constantly reconfigure your plugins simply isn’t worth it. The differences, when comparing these 6 plugins are marginal. But it can become a chasm depending on your ability to rightly configure the cache plugin in question.

Personally, if I read this article I’d look at the plugin screenshots check for the one that seems least daunting and try that one out first, provided it meets all my requirements. And then test your plugin. Compare it with similar websites that are known for their speed and popularity.

If you have very good results with your first try, you can stop there unless you need to specifically tackle a problem, like say a burst in traffic. Then it gets a bit trickier, I’d suggest strongly that you read  the benchmarking methods used on our tests before trying your own tests.

If you were wondering, Colorlib uses <a href="https://wordpress.org/plugins/w3-total-cache/" rel="nofollow external" data-wpel-link="external">W3TC</a> in combination with Memcached for object and database caching and CDN management.

For every website there is a speed ceiling and once you’ve hit it, you need to upgrade your hardware perhaps a top notch CDN service like <a href="https://colorlib.com/out/maxcdn" rel="nofollow external" data-wpel-link="external">MaxCDN</a> or <a href="https://www.cloudflare.com/" rel="nofollow external" data-wpel-link="external">CloudFlare</a>.

If you know of other awesome internal caching solutions, please do weigh in on the comments section. I look forward to reading them!

I hope that you found the post informative about the WordPress caching choices available to you. And that you’ve found the right plugin for your site.";s:10:"post_title";s:69:"Top 2 WordPress Caching Plugins to Make Your WordPress Website Faster";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:69:"top-2-wordpress-caching-plugins-to-make-your-wordpress-website-faster";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-05-08 02:42:43";s:17:"post_modified_gmt";s:19:"2018-05-08 02:42:43";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://innozilla.com/?p=375";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:4;O:8:"stdClass":23:{s:2:"ID";s:3:"417";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-06-04 05:40:36";s:13:"post_date_gmt";s:19:"2018-06-04 05:40:36";s:12:"post_content";s:10155:"<p class="single-first-p">How to boost your site with a CDN? As a web designer, you already know that great design is essential for a website—it’s the concept of design you trust, unless a website looks absolutely amazing, it’s going to be hard for new visitors to trust that site. That’s how essential great design is to a website.</p>
However, there’s another essential part of having a trustworthy website, the positive UX which comes with a fast, snappy website.

After great design, speed is one of the most important factors which contribute to the success of a website.
<h2>But Why is Speed so Important?</h2>
The <span class="ile-sp">importance of a fast website</span> has been researched over and over again.

Sites which are not fast enough create a negative perception, with the actual loading time of a site significantly affecting the conversion rates of websites.

As the loading time gets higher and higher, the conversion rate goes down significantly, with the optimum conversion rate happening at a page load time of 2.4 seconds.
<h2>What is a CDN and How Can it Help Speed Up My Sites?</h2>
Whilst there are many benefits to using a CDN which we will discuss shortly, there is one basic premise of how a CDN makes your site faster.

Simply put, a CDN is much better equipped as a network to handle the traffic of a website than most hosting services.

Shared hosting is typically optimized towards delivering a stable environment where your website can run PHP or other popular hosting environments.

It’s not geared towards optimizing for speed most times.

On the other hand, a CDN’s primary aim, and actual infrastructure setup is geared towards helping deliver a lightning fast website.

But how does a CDN actually speed up my site?
<h2>How a CDN’s Infrastructure Speeds Up Your Site</h2>
There are a few reasons why your website could be slow:
<ul>
 	<li>your shared hosting server is overwhelmed and responds slowly;</li>
 	<li>the images and content of your site are large and take a lot of time to download;</li>
 	<li>your website is using too many different scripts and images which are not optimized for a fast loading website;</li>
 	<li>the server location of your site is in a geographically different region than the visitors of your website.</li>
</ul>
There are other reasons, but these are the major ones.

You can address each and every one of these individually, we’ll focus mostly on the latter two here…
<h3>YOUR SHARED HOSTING SERVER IS OVERWHELMED AND RESPONDS SLOWLY</h3>
Shared hosting servers are not meant to be fast. They are meant to be affordable.

The economics of shared hosting means that to drive down the costs, the number of different websites hosted on the same server is significantly high.

That means, each time somebody visits your website, the hosting server is competing for resources with ALL of the websites hosted on the site, which means it typically takes more than a second to start serving your website.

Now, when we’re talking about making a website, a penalty of a second <em>before</em> we start doing any optimizations is a terrible way to start.

So a couple of recommendations:
<ol>
 	<li>If your website is hosted with WordPress, you need to find a <span class="ile-sp">reliable WordPress hosting company</span>, with great reviews, which is not cheap.</li>
 	<li>Opt for a higher payment plan, <span class="ile-sp">ideally a VPS</span>, such that your site will have enough resources and won’t be competing with hundreds of other sites</li>
</ol>
<h3>THE IMAGES OF YOUR SITE ARE LARGE</h3>
One of the largest impacts your site can have in terms of loading time, typically comes from the images hosted on your site.

You’ll find plenty of blogs touting the value of using images in your website and blog, and of course, this is excellent advice.

Images are necessary to break up large chunks of text and make for better readability.

Who also hasn’t heard of the phrase: “An image is worth a thousand words”

Yes, images are vital to the success of your site.

Yet, they have a drawback.

Unoptimized images can kill the loading time of your site.

Now, in an ideal world, we’d take the recommended approach of saving each file in a web-friendly format, optimizing large images and compressing them to a size which is acceptable without losing any of the quality.

Yet in reality, we simply don’t have the time or the inclination to go through an optimization process for each and every image.

But, there is a solution. Automation.

Once, again, CDNs come to the rescue. Image compression and optimization is typically a built-in feature of a CDN.

In essence, you go about your business of creating a great-looking website with awesome imagery, the CDN will handle the compression and optimization of the images.
<h3>YOUR WEBSITE USES A LOT OF SCRIPTS</h3>
This is another speed killer.

When we are just starting out as web designers, we enjoy discovering new plugins and testing them out and installing them on our website, never realising what the impact of the plugins will be.

Even established web designers tend to fall into this trap. Using tens of plugins to make sure the functionality required by the customer is delivered has its side effects.

In reality, each plugin you install on your site adds Javascript files, CSS files and requires more performance from your site.

You’ll see that in the form of a lot of requests on performance testing sites and a very-long loading time.

Again, a few recommendations:
<ol>
 	<li>Keep your site as lean as possible from plugins, less is more</li>
 	<li>Combine, compress and minify scripts</li>
 	<li>Enable HTTP/2</li>
</ol>
Whilst a CDN won’t help you decide which plugins to keep and which plugins to dump, a CDN is able to actually perform on-the-fly compression and minification of scripts, to make the total size of the content of your site smaller, and thus faster.

The third and very important recommendation is the setting of HTTP/2 – we’re not going to go into much detail, because we’ve already discussed HTTP/2 extensively, both <span class="ile-sp">on this site</span>and <span class="ile-sp">elsewhere</span>.

HTTP/2 has been written specifically to optimize the loading time of websites, particularly those websites which have a lot of different resources to serve.

Most CDN services allow you to quickly and easily enable HTTP/2 on your website giving your site an instant speed boost.

Even if you’ve done all of the above optimizations, there’s still one thing which can totally kill the speed of your website.

How do you fix that?
<h2>The Location of Your Website Server</h2>
Ok, if you’ve followed our advice your website should now be significantly faster than it was before.

But, there is one thing which can kill your website’s loading speed.

If your website is aimed at a local audience, your solution is simple: choose a good hosting service which is as physically close to your target location as possible.

However, this is more difficult if your if your website is catering for an international audience.

You simply can’t chose a server location which is physically close to <em>all</em> of your website visitors.

You can do the next best thing and host in the visitor location which is the most popular, but there’s a 2nd, more effective solution.

A CDN service is aimed specifically is fixing this problem.

A <span class="ile-sp">CDN’s infrastructure</span> is designed specifically to fix the problem which we have just described.

CDN’s have a network of hundreds of servers in tens of locations around the globe. These servers called edge or caching servers will replicate your images and static resources such as Javascript and CSS files to these locations.

When a user then hits your site, the heavy resources will get served from a location which is as physically close as possible to your visitor.

This reduces significantly the problem of distance and gives your site a very significant advantage in terms of loading speed.
<h2>How to Setup a Free CDN</h2>
The great thing about using a CDN, is that you can easily boost the speed of your website without having to pay anything extra, particular if your website is still growing.

Most CDN services offer a free plan, which will provide the essential caching functionality we discussed above. Typically, besides content optimization, you’ll also got a boost in your website’s security too, through the security mechanisms implemented by CDNs.

As your website grows and the needs of the site grow, you’ll then be able to upgrade to a plan which suits your needs better.

There are a couple of ways of setting up a CDN, this mostly depends on the actual CDN you will be using.
<h3>INSTALL A CDN PLUGIN</h3>
The first way of setting up a CDN is by using a CDN plugin. When setting up your CDN, you will get a URL which will be the new location of the static images of your site.

The CDN plugin will rewrite the URL of static resources such that they will be served from the CDN.

<i>https://www.example.com/images/logo-default.jpg</i>

is now rewritten as

<i>https://cdn.example.com/images/logo-default.jpg</i>

You’ll need to perform a few slight changes to the DNS entries of your site, such that cdn.example.com will resolve as the URL provided by your CDN.

You can use the CDN Enabler plugin if you’re using URL rewriting service such as KeyCDN (the authors of this plugin),  MaxCDN or Incapsula.

Once you’ve installed this plugin, the only thing you’ll need to do is enter the URL provided by your CDN service, and you should be good to go.
<h3>INSTALL A CDN AS A REVERSE PROXY</h3>
Another, better way of installing a CDN is using a reverse proxy.

This also requires minor changes to your DNS, which are typically specific to the CDN you will be installing.

This implementation is advantageous because it removes a significant load from hitting your server directly.";s:10:"post_title";s:26:"Boost your site with a CDN";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:26:"boost-your-site-with-a-cdn";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-04-08 05:49:10";s:17:"post_modified_gmt";s:19:"2019-04-08 05:49:10";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://innozilla.com/?p=417";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:5;O:8:"stdClass":23:{s:2:"ID";s:3:"436";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-06-14 06:47:48";s:13:"post_date_gmt";s:19:"2019-06-14 06:47:48";s:12:"post_content";s:8024:"Cloudflare is a CDN ( Content Delivery Network ) that offers more features than the Standard CDN can offer , It offers added security to DDoS attacks , It caches the static resources of your website making your site load faster &amp; putting less load on to the web hosting , It is the CDN which sits in the middle of your domain name &amp; web host .
<h2><strong>Cloudflare</strong></h2>
Cloudflare passes all the traffic which would hit your server to go through the filter process of Cloudflare and only the validated traffic is being routed to the host , You can have different security levels based on the spammers &amp; the hackers that you want to be filtering out.

It is the commercial content delivery network with the integrated distributed denial of service (DDoS) defense , It can act as a reverse proxy &amp; domain name server to your website , It offers a useful IPv6 transition mechanism if your hosting provider doesn’t present native IPv6.

It is the content delivery network which acts as a middle layer between your actual host &amp; user browsing site using your domain name , It is a free service , with premium packages for high traffic site , You can change your web-hosting without waiting for the name-servers to propagate , It has many data-centers around the world , So , your visitors will be served by the data-center that is nearest to them.

You can use Cloudflare when you are bottle-necked on the bandwidth or have too much spam or have the issues with DDoS or have the issues with the site getting hacked , If your host is low quality &amp; has the issues with the site load time or site getting offline too often , CloudFlare can store the static substances on your web-page like the pictures , JavaScript &amp; CSS , yet not HTML.

Content delivery network or CDN is different from Cloudflare , You can use alternative mirrored CDN url to serve the static content from the CDN , CDN serves the content from the nearest location from where the content is being requested which makes the content served faster , The browser don’t need to send extra header information for the static content and you can reduce the packet size to speed up your site’s loading time.

If you are serving the CDN content from the same domain/subdomain , CDN can free up your server from serving static content , it can serve the content from the nearest location to the end user , It allows the browsers to fetch the content parallel to your site’s content and it makes faster user experience.

You can go-to Cloudflare.com and signup for a free account , Add your domain and it will automatically detect current name-servers and IP address of current host , Just select the security &amp; cache settings you like and click next , Now copy the name-servers given by Cloudflare and add them in your domain registrar.
<h3><strong>Pros of using Cloudflare
</strong></h3>
Cloudflare hides your web-host’s original IP address so that any hacker can not attack the server , It caches your site’s some (or all) resources to load the site quicker &amp; making the actual server works less , It gives you the ability to block all the DDoS attacks by changing the site’s security.

As CloudFlare is like a CDN , it can continue to serve cached versions of most of your web-pages even if your website goes down , It enables you to add multiple number of domains , You can block access to your site for certain regions or countries , If you are receiving many attacks from particular countries , It gives you free SSL , which you can use on shared host as well.

Cloudflare is free to get started whereas the other CDN services are paid , As your site is being filtered and only genuine traffic is being passed , your site can save the resources as well as can increase the speed of your site because of the caching being performed by the Cloudflare.

You can filter out the bad traffic &amp; The website will be protected from the automated bots &amp; the spammers , Not all the traffic goes through the Cloudflare , so , you will save the bandwidth that you may waste due to the spammers &amp; the hackers , With Cloudflare , The stats are more accurate than JavaScript based stats because they catch all the traffic stats that JavaScript may miss because of blocked JavaScript content or pages not being loaded.

Cloudflare blocks DDoS and DoS , DDoS is an attack on the server to send too many automated requests to the server from various locations to bring it down , If you your IP Address is known to the hacker , Cloudflare can protect your server from DDoS as the attacker targets IP directly by passing the Cloudflare.

CloudFlare is a middleman between your server and your visitors , Sensitive data go through the CloudFlare server when they are delivered to a client , CloudFlare has the ability to monitor all your traffic , It can inject the code into your HTTP headers and your web pages and it can have any consequences.

DNS changes are faster , Your DNS is controlled by Cloudflare , Cloudflare mitigates DDOS attacks , It helps to reduce useless incoming traffic , Cloudflare offers free HTTPS , HTTP/2 &amp; SPDY certificates for your domain , It offers free ( HSTS ) HTTP Strict Transport Security for your website , It allows your website to be accessed through an IPV6 address even if your server has an IPV4 address , It can minify CSS , Javascript and HTML .

Cloudflare can protect your API by limiting the number of the requests for a given time using a rule , It is a paid option and it is very easy to setup from the Cloudflare configuration page , Cloudflare injects javascript into the code &amp; modifies the code of returning pages , It modifies the headers of pages , It can block one’s website if they choose to &amp; also monitor one’s visitors &amp; gather information about them.

Cloudflare is very easy to setup &amp; use , If you use WordPress or Drupal &amp; have access to your domain registrar ( to change your name-servers ) , The CDN will deliver cached images &amp; other bits of your website (but not the HTML) to your visitors from one of the several Cloudflare data-centers located around the world instead of from your web-server , It will optionally minify your HTML , Javascript &amp; CSS , The result is a measurable improvement in the perceived performance of your website.
<h2><strong>Cons of Cloudflare</strong></h2>
For some reasons , CloudFlare can slow down the page load rather than speeding it up , It is normal as a step (a hop) is added between your server &amp; a client , Since additional settings should be made on the CloudFlare website , The misconfiguration can lead to down times &amp; traffic drops .

Unless your site has decent amount of traffic &amp; you need the spam protection and have the issues with hacking or DDoS , Cloudflare had the issues with owners being blocked out of site , Cloudflare sometimes disallow access to the site , If it goes down , your site will go down , If you are looking for a way to make your blog or website more secure , speed it up , or make sure it is available .

If you are on shared hosting , you may need to check if your host supports Cloudflare or not though most shared hosts do not support Cloudflare , With Cloudflare , setting wildcard sub-domains are not possible , There is not enough information on the data that is cached .

Cloudflare offers limited security rules , Cloudflare’s basic/shared protection is great , but if you need custom page-rules you are limited to only 3 , If you are on a shared web-host , this is still a giant improvement over nothing , but if you are on a dedicated server running Mod_Security and integrated CSF , it’s very limiting .

Cloudflare has limited stats &amp; analytics , The threat &amp; attack statistics are very limited in detail – but still far better than nothing , The daily traffic reports are also limited , but you can still use your Google Analytics and this becomes a non-issue .";s:10:"post_title";s:24:"Cloudflare Pros and Cons";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:24:"cloudflare-pros-and-cons";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-06-14 06:51:19";s:17:"post_modified_gmt";s:19:"2019-06-14 06:51:19";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://innozilla.com/?p=436";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}s:8:"col_info";a:23:{i:0;O:8:"stdClass":13:{s:4:"name";s:2:"ID";s:7:"orgname";s:2:"ID";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:3;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:49699;s:4:"type";i:8;s:8:"decimals";i:0;}i:1;O:8:"stdClass":13:{s:4:"name";s:11:"post_author";s:7:"orgname";s:11:"post_author";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:49193;s:4:"type";i:8;s:8:"decimals";i:0;}i:2;O:8:"stdClass":13:{s:4:"name";s:9:"post_date";s:7:"orgname";s:9:"post_date";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:16513;s:4:"type";i:12;s:8:"decimals";i:0;}i:3;O:8:"stdClass":13:{s:4:"name";s:13:"post_date_gmt";s:7:"orgname";s:13:"post_date_gmt";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:129;s:4:"type";i:12;s:8:"decimals";i:0;}i:4;O:8:"stdClass":13:{s:4:"name";s:12:"post_content";s:7:"orgname";s:12:"post_content";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:11761;s:6:"length";i:4294967295;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:5;O:8:"stdClass":13:{s:4:"name";s:10:"post_title";s:7:"orgname";s:10:"post_title";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:69;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:6;O:8:"stdClass":13:{s:4:"name";s:12:"post_excerpt";s:7:"orgname";s:12:"post_excerpt";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:7;O:8:"stdClass":13:{s:4:"name";s:11:"post_status";s:7:"orgname";s:11:"post_status";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:7;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:16385;s:4:"type";i:253;s:8:"decimals";i:0;}i:8;O:8:"stdClass":13:{s:4:"name";s:14:"comment_status";s:7:"orgname";s:14:"comment_status";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:6;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:9;O:8:"stdClass":13:{s:4:"name";s:11:"ping_status";s:7:"orgname";s:11:"ping_status";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:4;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:10;O:8:"stdClass":13:{s:4:"name";s:13:"post_password";s:7:"orgname";s:13:"post_password";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:1020;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:11;O:8:"stdClass":13:{s:4:"name";s:9:"post_name";s:7:"orgname";s:9:"post_name";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:69;s:6:"length";i:800;s:9:"charsetnr";i:246;s:5:"flags";i:16393;s:4:"type";i:253;s:8:"decimals";i:0;}i:12;O:8:"stdClass":13:{s:4:"name";s:7:"to_ping";s:7:"orgname";s:7:"to_ping";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:13;O:8:"stdClass":13:{s:4:"name";s:6:"pinged";s:7:"orgname";s:6:"pinged";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:14;O:8:"stdClass":13:{s:4:"name";s:13:"post_modified";s:7:"orgname";s:13:"post_modified";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:129;s:4:"type";i:12;s:8:"decimals";i:0;}i:15;O:8:"stdClass":13:{s:4:"name";s:17:"post_modified_gmt";s:7:"orgname";s:17:"post_modified_gmt";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:129;s:4:"type";i:12;s:8:"decimals";i:0;}i:16;O:8:"stdClass":13:{s:4:"name";s:21:"post_content_filtered";s:7:"orgname";s:21:"post_content_filtered";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:4294967295;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:17;O:8:"stdClass":13:{s:4:"name";s:11:"post_parent";s:7:"orgname";s:11:"post_parent";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:49193;s:4:"type";i:8;s:8:"decimals";i:0;}i:18;O:8:"stdClass":13:{s:4:"name";s:4:"guid";s:7:"orgname";s:4:"guid";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:28;s:6:"length";i:1020;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:19;O:8:"stdClass":13:{s:4:"name";s:10:"menu_order";s:7:"orgname";s:10:"menu_order";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:11;s:9:"charsetnr";i:63;s:5:"flags";i:32769;s:4:"type";i:3;s:8:"decimals";i:0;}i:20;O:8:"stdClass":13:{s:4:"name";s:9:"post_type";s:7:"orgname";s:9:"post_type";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:4;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:16393;s:4:"type";i:253;s:8:"decimals";i:0;}i:21;O:8:"stdClass":13:{s:4:"name";s:14:"post_mime_type";s:7:"orgname";s:14:"post_mime_type";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:400;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:22;O:8:"stdClass":13:{s:4:"name";s:13:"comment_count";s:7:"orgname";s:13:"comment_count";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"innozilla";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:32769;s:4:"type";i:8;s:8:"decimals";i:0;}}s:8:"num_rows";i:6;s:10:"return_val";i:6;}